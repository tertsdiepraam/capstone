\chapter{Event Structures}%
\label{chap:event}

Event structures are used as an intermediate structure in the process of
attaching probabilities to Petri nets in the methods by \textcite{Varacca} and
\textcite{AB_branching_cells}. This chapter discusses event structures,
their connection to Petri nets and the concept of local finiteness.

\section{Preliminaries on Event Structures}

\begin{defn}[Event Structure]
  \nomevt{$\evtcausality$}{causality relation}
  \nomevt{$\conflict$}{conflict relation}
  An event structure is a triple
  \[ \EE  = \T{E, \evtcausality, \conflict} \]
  where $E$ is the set of \emph{events}, $\evtcausality$ is the \emph{causality}
  relation and $\conflict$ is the \emph{conflict} relation. An event structure
  $\EE$ must satisfy the following properties:
  \begin{itemize}
    \item the set $E$ is at most countable;
    \item the tuple $\T{E,\evtcausality}$ is a partial order such that for any
      event $e$ the set $\S{e'\in E\given e'\evtcausality e}$ is finite and
    \item the relation $\conflict$ is symmetric and irreflexive, and satisfies
      \[ \Forall{x,y,z \in E} \T{ x\conflict y \wedge y\evtcausality z } \implies x\conflict z. \]
  \end{itemize}
\end{defn}

\begin{defn}[Prefix \& Configuration]
  A subset $A\subseteq E$ is called a \emph{prefix} if it is downwards closed,
  that is, if
  \[ \Forall{x\in E}\Forall{y\in A}\quad x\evtcausality y\implies x\in A. \]
  Additionally, a prefix $v$ is called a \emph{configuration} if it is
  \emph{conflict-free}:
  \[ \conflict\intersect\,\T{v\times v} = \emptyset. \]
\end{defn}

A configuration $v$ represents a valid execution of the system, since each event
in $v$ only depends on events in $v$ and no two events in conflict can be in the
same configuration. Therefore, there is a valid order in which the events in $v$
can be executed.

We say that two configurations $x$, $y$ are \emph{compatible} if $x\union y$ is
a configuration. We denote the set of finite configurations with
$\mathcal{V}_\EE$.\nomevt{$\mathcal{V}_\EE$}{finite configurations of $\EE$} The
elements of $\mathcal{V}_\EE$ can be partially ordered based on the events they
include. Additionally, we define the set of maximal configurations
$\Omega_\EE$.\nomevt{$\Omega_\EE$}{maximal configurations of $\EE$} By applying
Zorn's lemma on the partial order $\T{\mathcal{V}_\EE,\subseteq}$, we conclude
that this set is non-empty~\cite{Abbes}.

A subset of events $F\subseteq E$ induces a sub-event structure
$\T{F,\evtcausality_F,\conflict_F}$ with
\[
  \evtcausality_F\,=\,\evtcausality\intersect\,\T{F\times F},\quad
  \conflict_F = \conflict\intersect\,\T{F\times F}.
\]

To denote the finite and maximal configurations of this event structure, abusing
notation we will freely write $\mathcal{V}_F$ and $\Omega_F$, respectively,
without explicitly stating the event structure.

Given an event structure $\EE=(E,\evtcausality,\conflict)$, The smallest
configuration containing the element $e\in E$ is denoted
\nomevt{$\config{e}$}{smallest configuration containing $e$}
\[ \config{e} \deq \S{e'\in E \given e'\evtcausality e}. \]

Building on this definition, the smallest configuration enabling $e$ is denoted\nomevt{$\configenable{e}$}{smallest configuration enabling $e$}
\[ \configenable{e} \deq \config{e}\diff \S{e}. \]

\begin{defn}[Immediate conflict]
  \nomevt{$\iconflict$}{immediate conflict relation}
  The \emph{immediate conflict} relation $\iconflict$ on $E$ is defined as
  \[ \Forall{e,e'\in E}\qquad e\iconflict e' \text{ iff
    } \T{[e]\times[e']}\intersect\conflict = \S{\T{e,e'}}. \]
\end{defn}

Immediate conflict is analogous to direct conflict in Petri nets, as it
describes where choices must be made in the execution of the event structure.

An example of the graphical representation of event structures is given in
\cref{fig:event_struct}. Events are represented by a dot ($\bullet$). Causality
is given by the transitive reflexive closure of the arrows
($\xymatrix@1{\ar[r] &}$) and immediate conflict is denoted by wavy lines
($\xymatrix@1{\ar@{~}[r] &}$).

\begin{figure}
  \[
    \xymatrix{
      \event \ar[d] \nameleft u\\
      \event \ar@{~}[r] \nameleft v & \event \nameright w}
  \]
  \caption{An example of an event structure. It corresponds to the asymmetric
    confusion Petri net in \cref{fig:aconf}.}%
  \label{fig:event_struct}
\end{figure}


\begin{defn}[Future of a configuration]
  \nomevt{$\EE^v$}{future of the configuration $v$}
  Let $\EE=(E,\evtcausality,\conflict)$ be an event structure. Call the
  \emph{future} of a configuration $v$ the event structure $\EE^v$ induced by
  \[ E^v\deq \S{e\in E\diff v \given \Forall{e'\in v}\neg\T{e\conflict e'}}, \]
  which is the set of events not in conflict with any of the events in the
  configuration and not in the configuration itself.
\end{defn}

\section{From Petri Nets to Event Structures}%
\label{sec:petri_to_event}
Petri nets and event structures are both models for concurrent processes.
However, event structures lack a representation of the state of a system, since
the only atomic elements are events, which are analogous to transitions.
Nevertheless, Petri nets can be mapped to event structures by using occurrence
nets as an intermediate step.

Let $N=\T{ P,T,F }$ be a $1$-safe Petri net. As shown by
\textcite[Thm.~4.7]{Winskel_unfolding}, $N$ has an unfolding $\T{ U, \rho }$,
where $U=\T{ P', T', F' }$ is an occurrence net. \textcite{Nielsen} have
defined the map that converts an occurrence net to an event structure:
\[ \xi(U) \deq \T{T',\ F^*\intersect \T{T'\times T'},\ \conflict_U\intersect\,\T{T'\times T'}}. \]

\Cref{fig:shopocc} shows the unfolding of the Petri net in \cref{fig:shop}. The
event structure obtained by applying $\xi$ to the unfolding is shown in
\cref{fig:shopevt}.

\begin{figure}
  \begin{subfigure}[b]{.47\linewidth}
    \centering
    \begin{tikzpicture}
      \p{s1}{2,9}

      \p{a1}{0,11}
      \t[$t_1$]{a2}{0,10}
      \p{a3}{0,9}
      \t[$t_3$]{sa1}{1,8}
      \p{sa2}{1,7}
      \t[$t_4$]{sa3}{1,6}
      \p{a4}{1,5}
      \p{s2}{1,4}
      \t[$t_3$]{sa4}{0,3}
      \p{sa5}{0,2}
      \t[$t_4$]{sa6}{0,1}
      \p{a5}{0,0}

      \p{b1}{4,11}
      \t[$t_2$]{b2}{4,10}
      \p{b3}{4,9}
      \t[$t_5$]{sb1}{3,8}
      \p{sb2}{3,7}
      \t[$t_6$]{sb3}{3,6}
      \p{b4}{3,5}
      \p{s3}{3,4}
      \t[$t_5$]{sb4}{4,3}
      \p{sb5}{4,2}
      \t[$t_6$]{sb6}{4,1}
      \p{b5}{4,0}

      \flow{
        a1/a2, a2/a3, a3/sa1, s1/sa1, sa1/sa2, sa2/sa3, sa3/a4, sa3/s3,
        s2/sa4, a3/sa4, sa4/sa5, sa5/sa6, sa6/a5,
        b1/b2, b2/b3, b3/sb1, s1/sb1, sb1/sb2, sb2/sb3, sb3/b4, sb3/s2,
        s3/sb4, b3/sb4, sb4/sb5, sb5/sb6, sb6/b5%
      }
      \marking{a1, b1, s1}
    \end{tikzpicture}
    \caption{The occurrence net corresponding to the Petri net in
      \cref{fig:shop}.}%
    \label{fig:shopocc}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{.47\linewidth}
    \[
      \xymatrix{
        &\event \ar[d] \ar@/_2pc/[dddl]\nameleft{t_1} & \event \ar[d] \ar@/^2pc/[dddr] \nameright{t_2} &\\
        &\event \ar[d] \ar@{~}[ddl]\ar@{~}[r] \nameleft{t_3} & \event \ar[d] \ar@{~}[ddr]\nameright{t_5} &\\
        &\event \ar[drr] \namedown{t_4} & \event \ar[dll] \namedown{t_6} &\\
        \event \ar[d] \nameleft{t_3} & & &\event \ar[d] \nameright{t_5} \\
        \event \nameleft{t_4} & & &\event \nameright{t_6}}
    \]
    \caption{The event structure corresponding to the occurrence net in
      \cref{fig:shopocc}.}%
    \label{fig:shopevt}
  \end{subfigure}
  \caption{The occurrence net and event structure corresponding to the Petri net
    in \cref{fig:shop}.}%
  \label{fig:shop_to_pes}
\end{figure}

\section{Confusion in Event Structures}
The notion of confusion can also be translated from the theory of Petri nets to
event structures. This will be useful for some of the definitions later on.

In the symmetric case of confusion, there exist three transitions $t$, $u$ and
$v$ which we will directly map to three events $t$, $u$ and $v$. Since
$\pre t\intersect\pre u \neq \emptyset \neq \pre u\intersect v$ in the Petri net
model, we conclude that $t\iconflict u$ and $u\iconflict v$. Furthermore, we see
that $\neg(t\iconflict v)$, since $\pre t\intersect\pre v = \emptyset$. The
condition for symmetric confusion is then
\[ \Exists{t,u,v\in E}\quad t\iconflict u \wedge u\iconflict v\wedge \neg(t\iconflict v).\]
In other words, the immediate conflict relation must be transitive for the Petri
net to be confusion-free.

Using similar reasoning as before, we can see that there are two events $u$ and
$v$ such that $u\iconflict v$ in the asymmetric case of confusion. Furthermore,
there is some event $t$ that is maximal in $\configenable{u}$ and
$t\notin\configenable{v}$. By \cref{prop:equal_iff_max_equal}, the
configurations $\configenable{u}$ and $\configenable{v}$ are equal if and only
if $\max(\configenable{u})=\max(\configenable{v})$. Therefore, the condition for
asymmetric confusion is
\[ \Exists{u,v\in E}\quad u\iconflict v\wedge \configenable{u}\neq\configenable{v}. \]
The corresponding condition for an event structure to be confusion-free is then
that
$\Forall{u,v\in E}\quad u\iconflict v\implies \configenable{u}=\configenable{v}$.

\begin{thm}[Confusion]%
  \label{thm:eventconfusion}
  An event structure $\EE=(E,\evtcausality,\conflict)$ has confusion if at least
  one of the following conditions holds:
  \begin{enumerate}
    \item (Symmetric)
      $\Exists{t,u,v\in E} t\iconflict u\wedge u\iconflict v\wedge\neg(t\iconflict v)$;
    \item (Asymmetric)
      $\Exists{u,v\in E} u\iconflict v \wedge \configenable{u}\neq\configenable{v}$.
  \end{enumerate}
\end{thm}

Examples of confused event structures are shown in \cref{fig:eventconfusion},
which are direct translations from the Petri nets in \cref{fig:basicconf}.

\begin{figure}
  \begin{subfigure}[b]{.47\linewidth}
    \[
      \xymatrix{
        \event \ar@{~}[r] & \event \ar@{~}[r] & \event
      }
    \]
    \caption{Symmetric confusion}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{.47\linewidth}
    \[
      \xymatrix{
        \event \ar[d] & \\
        \event \ar@{~}[r] & \event
      }
    \]
    \caption{Asymmetric confusion}
  \end{subfigure}
  \caption{The minimal confused event structures.}%
  \label{fig:eventconfusion}
\end{figure}

\section{Locally Finite Event Structures}
The concept of locally finite event structures was introduced
by~\textcite{AB_branching_cells} to describe the class of event structures, and
by extension Petri nets, that their method applies to. Intuitively, an event
structure is locally finite if for every event $e$ there is a finite number of
events that directly or indirectly influence whether $e$ can be fired.

\begin{defn}[Stopping prefix]%
  \label{defn:stopping_prefix}
  A prefix $B$ of $\EE$ is called \emph{stopping} if it is closed under
  immediate conflict.
\end{defn}

\begin{defn}[Locally finite]
  An event structure $\EE=(E,\evtcausality,\conflict)$ is called
  \emph{locally finite} if for each event $e\in\EE$, there exists a finite
  stopping prefix containing $e$.
 
  A Petri net is called locally finite if the event structure $\xi(U)$
  corresponding to its unfolding $U$ is locally finite.
\end{defn}

The difference between a locally finite and a non-locally finite event
structure is illustrated in \cref{fig:locfin_vs_nonlocfin}. In \cref{fig:locfin},
the right-most event is in immediate conflict with infinitely many events in the
middle column. Any finite prefix containing that event is therefore not stopping
since it would not be closed under immediate conflict. In \cref{fig:nonlocfin},
every event is only in immediate conflict with two other events.

\begin{figure}[htbp]
  \begin{subfigure}[t]{.47\linewidth}
    \[
      \xymatrix{
        \event \ar[d] \ar[dr] \ar[drr] \ar@{~}[r] \ar@/_/@{~}[rr]& \event \ar@{~}[r] & \event \\
        \event \ar[d] \ar[dr] \ar[drr] \ar@{~}[r] \ar@/_/@{~}[rr]& \event \ar@{~}[r] & \event \\
        \event \ar@{~}[r] \ar@/_/@{~}[rr] \namedown{\vdots}
        & \event \ar@{~}[r] \namedown{\vdots}
        & \event \namedown{\vdots} \\
      }
    \]
    \caption{A locally finite event structure.}%
    \label{fig:nonlocfin}
  \end{subfigure}
  \begin{subfigure}[t]{.47\linewidth}
    \[
      \xymatrix{
        \event \ar[d] \ar[dr] \ar@{~}[r]
        & \event \ar@{~}[r]
        & \event \ar@{~}[ld] \ar@{~}[ldd]\\
        \event \ar[d] \ar[dr] \ar@{~}[r]
        & \event \\
        \event \ar@{~}[r]\namedown{\vdots}
        & \bullet \namedown{\vdots}
      }
    \]
    \caption{A non-locally finite event structure.}%
    \label{fig:locfin}
  \end{subfigure}
  \caption{Two similar event structures: one locally finite and one non-locally
finite.}%
  \label{fig:locfin_vs_nonlocfin}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Capstone/capstone"
%%% TeX-command-extra-options: "--shell-escape"
%%% End:

% LocalWords:  irreflexive
