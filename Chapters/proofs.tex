\chapter{Proofs}%
\label{chap:proofs}

\begin{prop}\label{prop:remofdependence}
\textcite{Montanari} define the operation $N\ominus p$ as the least set that
satisfies the rules:
\[
  \frac{q\in(\init{N})\diff\Set{p}}{q\in N\ominus p},
  \qquad
  \frac{t\in N\quad\pre{t}\subseteq N\ominus p}{t\in N\ominus p},
  \qquad
  \frac{t\in N\ominus p\quad q\in\post{t}}{q\in N\ominus p},
\]
where $N=(P,T,F)$ as usual. For $p\in\init N$, this definition is equivalent to the definition
given in \cref{def:remofdependence}:
\[ N\ominus p \deq N \diff \S{n\in P\union T \given p\dep n}, \]
where
\begin{align*}
  \dep_0 &\deq \Id \\
  \dep_{n+1} &\deq \begin{aligned}[t]
    \dep_{n}
    &\union \S{(x,p) \in (P\union T)\times P \given \pre p \neq\emptyset \wedge \Forall{t\in\pre p} x\dep_n t} \\
    &\union \S{(x,t) \in (P\union T)\times T \given \Exists{p\in\pre t} x\dep_n p}
  \end{aligned} \\
  \dep &\deq \Union_{n\in\NN} \dep_n.
\end{align*}
\end{prop}

\begin{proof}
 \textcite{Montanari} define the operation $N\ominus p$ as the least set that
 satisfies the rules:
 \[
   \frac{q\in(\init{N})\diff\Set{p}}{q\in N\ominus p},
   \qquad
   \frac{t\in N\quad\pre{t}\subseteq N\ominus p}{t\in N\ominus p},
   \qquad
   \frac{t\in N\ominus p\quad q\in\post{t}}{q\in N\ominus p},
 \]
 where $N=(P,T,F)$ as usual. Recall that the definition given in the main text
 is
 \[ N\ominus p \deq N \diff \S{n\in P\union T \given p\dep n}, \]
 where
 \begin{align*}
   \dep_0 &\deq \Id \\
   \dep_{n+1} &\deq \begin{aligned}[t]
     \dep_{n}
     &\union \S{(x,p) \in (P\union T)\times P \given \pre p \neq\emptyset \wedge \Forall{t\in\pre p} x\dep_n t} \\
     &\union \S{(x,t) \in (P\union T)\times T \given \Exists{p\in\pre t} x\dep_n p}
   \end{aligned} \\
   \dep &\deq \Union_{n\in\NN} \dep_n.
 \end{align*}

 To show the equivalence it suffices to show that under our definition,
  \begin{align*}
    \text{(1)}\quad&\Forall{q\in P}\quad &&q\in N\ominus p \iff q\in(\init N)\diff\S{p}
    \vee\Exists{t\in T}\T{t\in N\ominus p\wedge q\in\post t}, \\
    \text{(2)}\quad&\Forall{t\in T}\quad &&t\in N\ominus p \iff t\in N \wedge \pre t\subseteq N\ominus p.
  \end{align*}
  The first condition can be split up since the operands of $\vee$
  are mutually exclusive, since $q\in\init N \iff \neg\Exists{t\in T} q\in\post t$.
  Therefore we have to prove
  \begin{align*}
    \text{(1a)}&\quad\Forall{q\in\init N} &&\quad q\in N\ominus p \iff q\in(\init N)\diff\S{p}, \\
    \text{(1b)}&\quad\Forall{q\in P\diff\init N} &&\quad q\in N\ominus p \iff \Exists{t\in T} \T{t\in N\ominus p\wedge q\in \post t}.
  \end{align*}

  In order to prove 1a, consider a place $q\in\init N$. Then either $p=q$ or
  $q\in\init N\diff\S{p}$. If $p=q$ then $p\dep_0 q$, implying that $p\dep q$
  and $p\notin N\ominus p$. If $q\in\init N\diff\S{p}$ then $\neg(p\dep_0 q)$
  and $\pre q=\emptyset$, so the condition for including $(p,q)$ in $\dep_{n+1}$
  is always false. Therefore $\neg\Exists{n\in\NN} p\dep_n q$, implying that
  $\neg(p\dep q)$ and finally $q\in N\ominus p$. Therefore 1a holds.

  For 1b, consider a place $q\in P\diff\init N$. Assume a transition
  $t$ such that $t\in N\ominus p$ and $q\in\post t$. This implies that
  $\neg(p\dep t)$ and $\neg\Exists{n\in\NN} p\dep_n t$. Since $t\in\pre q$,
  there does not exists a $n\in\NN$ such that $\Forall{t\in\pre p} p\dep_n t$,
  therefore $\neg\Exists{n\in\NN} p\dep_n t$, which means that
  $\neg(p\dep_n q)$. Now assume the opposite we get:
  \begin{align*}
    &\neg\Exists{t\in T} \T{t\in N\ominus p \wedge q\in\post t} \\
    \therefore\quad &\Forall{t\in T}t\notin N\ominus p\vee q\notin\post t \\
    \therefore\quad &\Forall{t\in\pre q} t\notin N\ominus p \\
    \therefore\quad &\Forall{t\in\pre q} p\dep t \\
    \therefore\quad &\Forall{t\in\pre q} \Exists{n\in\NN} p\dep_n t \\
    \therefore\quad &\Exists{n\in\NN} \Forall{t\in\pre q} p\dep_n t \\
    \therefore\quad &\Exists{n\in\NN} \pre q\neq\emptyset \wedge \Forall{t\in\pre q} p\dep_n t \\
    \therefore\quad &\Exists{n\in\NN} p\dep_{n+1} q \\
    \therefore\quad &p\dep q\\
    \therefore\quad &q\notin N\ominus p
  \end{align*}
  Therefore, both directions of the bi-implication 1b holds.

  We apply a similar technique for 2, considering a transition $t\in T$. First
  assume $\pre t\subseteq N\ominus p$. This implies
  \begin{align*}
    &\Forall{q\in\pre t} q\in N\ominus p\\
    \therefore\quad &\Forall{q\in\pre t} \neg(p\dep q) \\
    \therefore\quad &\Forall{q\in\pre t} \neg\Exists{n\in\NN} p\dep_n q \\
    \therefore\quad &\neg\Exists{q\in\pre t} \Exists{n\in\NN} p\dep_n q \\
    \therefore\quad &\neg\Exists{n\in\NN} \Exists{q\in\pre t} p\dep_n q \\
    \therefore\quad &\neg\Exists{n\in\NN} p\dep_n t \\
    \therefore\quad &\neg(p\dep t) \\
    \therefore\quad &t\in N\ominus p.
  \end{align*}
  And for the other direction we assume $\pre t\nsubseteq N\ominus p$, which
  implies
  \begin{align*}
    &\Exists{q\in\pre t} q\notin N\ominus p \\
    \therefore\quad &\Exists{q\in\pre t} p\dep q \\
    \therefore\quad &\Exists{q\in\pre t} \Exists{n\in\NN} p\dep_n q \\
    \therefore\quad &\Exists{n\in\NN} \Exists{q\in\pre t} p\dep_n q \\
    \therefore\quad &\Exists{n\in\NN} p\dep_{n+1} t \\
    \therefore\quad &p\dep t \\
    \therefore\quad &t\notin N\ominus p.
  \end{align*}
  Now both directions of the bi-implication of 2 are proven, which proves all of
  the statements that needed to be proven.
\end{proof}

\begin{thm}[Confusion-free cells,~\cite{Varacca}]
  An event structure is confusion-free if and only if its cells are closed under conflict.
\end{thm}

\begin{proof}
  Let $\EE=\T{E,\leq,\conflict}$ be an event structure. We first prove that the
  cells of an event structure containing confusion are not closed under
  immediate conflict. Consider the symmetric case of confusion, such that
  \begin{equation}%
    \label{eq:symconf}
    \Exists{t,u,v\in E}\quad
    t\iconflict u\wedge u\iconflict v\wedge \neg\T{t\iconflict v}.
  \end{equation}
  Therefore, any cell $c$ closed under $\iconflict$ and containing $t$, $u$ or $v$
  must contain all three. But $\neg\T{t\iconflict v}$, so $t$ and $v$ cannot
  both be in the cell. Therefore the cell cannot be closed.

  Now consider the asymmetric case of confusion, such that
  \begin{equation}%
    \label{eq:asymconf}
    \Exists{t,u,v\in E}\quad
    t\preceq u\wedge \neg\T{t\preceq v}\wedge u\iconflict v.
  \end{equation}
  Then $u$ and $v$ cannot be in the same cell, since $t\in\configenable{u}$ and
  $t\in\configenable{v}$. Therefore a cell containing $u$ or $v$ is not closed
  under $\iconflict$.

  To prove that the existence of a cell not closed under $\iconflict$ implies
  that the net has confusion, we assume there exists a cell $c$ that is not
  closed under $\iconflict$. Then, there must exists two events $t$ and $u$ such
  that $t\iconflict u$ such that $t\in c$ and $u\notin c$. Therefore, one of two
  conditions must hold: either
  $\Exists{x\in c} \neg\T{x\iconflict u}$ or
  $\Exists{x\in c} \configenable{x}\neq\configenable{u}$. In the first case,
  \cref{eq:symconf} holds, therefore the net is symmetrically confused.

  Assume the first condition does not hold, such that
  $\Forall{x\in c} x\iconflict u$. Now the second condition must hold, therefore
  we conclude
  $\Exists{x\in c} x\iconflict u \wedge \configenable{x}\neq\configenable{u}$.
  Call the $x$ for which this holds $v$. We will now show that there must be a
  maximal element in either $\configenable{u}$ or $\configenable{v}$ that is not
  in the other. Let
  \[\max(S) \deq \S{x\in S \given \neg\Exists{y\in S} x\preceq y}\]
  denote the set of maximal elements of a partially ordered set. Note that
  \[ \configenable{x} = \Union_{y\in\max(\configenable{x})}\config{y}.\]
  Therefore, if $\max(\configenable{u})=\max(\configenable{v})$ then
  $\configenable{u}=\configenable{v}$, so there must be an element of
  $\configenable{u}$ or $\configenable{v}$ that is not in the other.

  Call this element and, without loss of generality, assume
  $s\in\max(\configenable{u})$. Therefore $s\preceq u$ and $\neg\T{s\preceq v}$.
  Then finally \cref{eq:asymconf} (substituting $s$ for $t$) holds and the
  net is asymmetrically confused. Therefore the net is confused if there is a
  cell that is not closed under $\iconflict$.
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Capstone/capstone"
%%% TeX-command-extra-options: "--shell-escape"
%%% End:
