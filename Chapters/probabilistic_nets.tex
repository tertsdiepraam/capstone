\chapter{Randomizing Confusion-Free Event Structures}%
\label{chap:probability}
This chapter discusses a method of attaching local probabilities to
confusion-free event structures introduced by \textcite{Varacca}. Then, we
attempt to apply the same method to confused event structures to understand why
the method fails. The limitations of this method motivate the branching cells
method described in \cref{chap:branchingcells}.

\section{Probabilistic Event Structures}
With the definitions of event structures in place, \emph{probabilistic event
structures with independence} can be defined. This is done in the way described
in~\cite{Varacca}. The idea here is to define probability to local parts of the
event structure that generate a sensible probability distribution for the
configurations. We will first only consider confusion-free event structures.

To simplify some upcoming definitions, the concept of a \emph{covering} is
introduced.

\begin{defn}[Covering]%
  \label{defn:covering}
  Let $x,x'\in\mathcal{V}_\EE$ be two configurations. We say that $x$
  \emph{covers} $x'$ if there exists an event $e\notin x'$ such that
  $x = x'\union\S{e}$. A covering at $x$ is a maximal non-empty set of
  pairwise incompatible configurations that cover $x$.
\end{defn}

The previously mentioned ``sensible'' probability for the configurations must
satisfy three conditions: normality, conservation and independence. Such a
probability distribution is called a \emph{configuration valuation with
independence}.

\begin{defn}[Configuration valuation with independence]%
  \label{defn:configval}
  A mapping $v\,:\,\mathcal{V}_\EE\to [0,1]$ is called a
  configuration valuation with independence if it satisfies
  \begin{enumerate}
    \item (Normality) $v(\emptyset)=1$;
    \item (Conservation) if $C$ is a covering at $x$, then $\sum_{c\in C}v(c)=v(x)$;
    \item (Independence) if $x$ and $y$ are compatible then
      $v(x)\cdot v(y)= v(x\union y)\cdot v(x\intersect y)$.
  \end{enumerate}
\end{defn}

The first condition simply states that the empty configuration must always
occur, since it is the starting point of every configuration. The second condition
implies that the sum of the probabilities of the maximal configurations is $1$.
Finally, the condition of independence states that the valuations for compatible
configurations must be probabilistically independent.

To create a \emph{probabilistic event structure with independence}, we attach a
configuration valuation with independence to an event structure.

\begin{defn}[Probabilistic event structure with independence]%
  \label{defn:probwithindependence}
  A probabilistic event structure with independence is a pair $(\EE,v)$
  with a confusion-free event structure $\EE$ and a configuration valuation $v$.
\end{defn}

Now we define local probability distributions to match the global probability
distribution defined above. To this end, \emph{cells} are defined to represent
the sets of events where choices have to be made. The local probability
distributions of the event structure will be attached to each of these cells.

\begin{defn}[Cell]%
  \label{defn:cell}
  A cell is a maximal non-empty set $c$ of events such that $e,e'\in c$ implies
  $e\iconflict e'$ and $\configenable{e}=\configenable{e'}$.
\end{defn}

\textcite[Definition~2.3]{Varacca} give a different yet equivalent definition of
confusion-freeness based on cells. Here, this definition is treated as a
theorem following from \cref{thm:eventconfusion}.

\begin{thm}[Confusion-free]
  An event structure is confusion-free if and only if its cells are closed under
  immediate conflict.
\end{thm}

\begin{proof}
  Let $\EE=\T{E,\leq,\conflict}$ be an event structure. We first prove that the
  cells of an event structure with confusion are not closed under immediate
  conflict. Consider the symmetric case of confusion, such that
  \begin{equation}%
    \label{eq:symconf}
    \Exists{t,u,v\in E}\quad
    t\iconflict u\wedge u\iconflict v\wedge \neg\T{t\iconflict v}.
  \end{equation}
  Therefore, a cell $c$ containing $t$ cannot contain $v$ since
  $\neg\T{t\iconflict v}$. Hence, $c$ is not closed under $\iconflict$.

  In the asymmetric case of confusion, we have
  \begin{equation}%
    \label{eq:asymconf}
    \Exists{u,v\in E}\quad
    u\iconflict v \wedge \configenable{u}\neq\configenable{v}.
  \end{equation}
  Therefore, a cell $c$ containing $u$ cannot contain $v$, since
  $\configenable{u}\neq\configenable{v}$. Hence, $c$ is not closed under
  $\iconflict$.

  Now we prove that the existence of a cell which is not closed under
  $\iconflict$ implies that the net has confusions, we assume that there exists
  a cell $c$ that is not closed under $\iconflict$. Then, there must exist two
  events $t$ and $u$ such that $t\iconflict u$ with $t\in c$ and $u\notin c$.
  Therefore, one of two conditions must hold:
  $\Exists{x\in c} \neg\T{x\iconflict u}$ or
  $\Exists{x\in c} \configenable{x}\neq\configenable{u}$. In the first case,
  \cref{eq:symconf} holds, therefore the net is symmetrically confused.

  Assume the first condition does not hold, so
  $\Forall{x\in c} x\iconflict u$. Now the second condition must hold, therefore
  we conclude
  $\Exists{x\in c} x\iconflict u \wedge \configenable{x}\neq\configenable{u}$.
  Therefore \cref{eq:asymconf} holds and the net is asymmetrically confused.
\end{proof}

A \emph{cell valuation} then attaches a probability to each of the choices in
the cell.

\begin{defn}[Cell valuation]%
  \label{defn:cellval}
  A cell valuation on a confusion-free event structure
  $\EE=\T{E,\evtcausality,\conflict}$ is a mapping $p\,:\,E\to [0,1]$ such that for
  every cell $c$, we have $\sum_{x\in c}p(x) = 1$.
\end{defn}

These local probabilities then give rise to a configuration valuation by
multiplying the probabilities of the events in the configurations.

\begin{prop}[\protect{\textcite[Prop.~2.8]{Varacca}}]%
  \label{prop:celltoconfig}
  Let $p$ be a cell valuation and $v\,:\,\mathcal{V}_\EE\to [0,1]$ be mapping
  given by $v(x)=\prod_{e\in x}p(e)$. Then $v$ satisfies normality, conservation
  and independence. Therefore, $v$ is a configuration valuation with
  independence.
\end{prop}

\begin{prop}[\protect{\textcite[Prop.~2.11]{Varacca}}]%
  \label{prop:configtocell}
  Let $v$ be a configuration valuation with independence. Then there exists a
  cell valuation $p$ such that $v(c) = \prod_{e\in x}p(e)$.
\end{prop}

As an example of a cell valuation, consider the net in
\cref{fig:cellvaluation}. To create a valid cell valuation for this event
structure, the probabilities for the events in each cell must add up to $1$. For
example, we can attach the probabilities given in the table in
\cref{fig:cellvaluation}. To determine the configuration valuation we then
multiply the probabilities of the events in the configuration. For example, the
configuration $\S{a,d}$ has the valuation
\[ v(\S{a,d}) = p(a) \cdot p(d) = \frac{3}{4}\cdot\frac{1}{2} = \frac{3}{8}. \]
The only covering for this configuration then consists of the configurations
$\S{a,d,e}$ and $\S{a,d,f}$. The valuation of these configurations are
\[ v(\S{a,d,e}) = \frac{1}{8}\quad\text{and}\quad v(\S{a,d,f}) = \frac{2}{8}, \]
which indeed add up to $v(\S{a,d})$, as required by the condition of
conservation. Furthermore, if we take the compatible configurations $v(\S{a,f})$
and $v(\S{a,c})$, we have that
\[ v(\S{a,f})\cdot v(\S{a,c}) = \frac{3}{4}\cdot\frac{2}{3}\cdot\frac{3}{4}\cdot\frac{1}{2} = \frac{3}{16} \]
and
\begin{align*}
  v(\S{a,c}\union\S{a,f})\cdot v(\S{a,c}\intersect\S{a,f})
  &= v(\S{a,c,f})\cdot v(\S{a})\\
  &= \frac{3}{4}\cdot\frac{2}{3}\cdot\frac{1}{2}\cdot\frac{3}{4} = \frac{3}{16},
\end{align*}
as required by the condition of independence.

\begin{figure}[htbp]
  \begin{subfigure}{.47\linewidth}
    \[
      \xymatrix{
        & \event\ar@{~}[r]\ar[d]\ar[dl]\nameup{a}
        & \event\ar[d]\nameup{b}
        \POS[]-<1.5pc,0pc> *+=<3.75pc,1pc>[F--]{}
        & \event\ar@{~}[r]\ar[dl]\nameup{c}
        & \event\nameup{d}
        \POS[]-<1.5pc,0pc> *+=<3.75pc,1pc>[F--]{}
        \\
        \event\ar@{~}[r]\namedown{e}
        & \event\namedown{f}
        \POS[]-<1.5pc,0pc> *+=<3.75pc,1pc>[F--]{}
        & \event\namedown{g}
        \POS[]-<0pc,0pc> *+=<1pc,1pc>[F--]{}
      }
    \]
  \end{subfigure}
  \begin{subfigure}{.47\linewidth}
    \[
      \begin{array}{c|ccccccc}
        x & a & b & c & d & e & f & g \\
        \hline
        &&&&&&&\\[-0.8em]
        p(x) & \frac{3}{4} & \frac{1}{4} & \frac{1}{2} & \frac{1}{2} & \frac{1}{3} & \frac{2}{3} & 1 \\
      \end{array}
    \]
  \end{subfigure}
  \caption{Confusion-free net with cells represented as dashed rectangles and an
    example of a cell valuation for this net.}%
  \label{fig:cellvaluation}
\end{figure}

\section{Confusion in Probabilistic Event Structures}
The approach discussed in the previous section works well for confusion-free
events structures, but the case of confusion still has to be discussed.

Consider the nets in \cref{fig:cellsconfusion}. For the symmetric case of
confusion, the cells overlap, meaning that the probabilities on the left and
right event must be equal to each other. While possibly inconvenient, this does
not directly pose a problem. Instead, the problem is that the condition of
conservation is not satisfied. Assume that the events have the non-zero
valuations $p(t)$, $p(u)$ and $p(v)$ and consider the configuration $\S{t}$ with
the configuration valuation $v(\S{t}) = p(t)$. The only configuration in the
covering at $\S{t}$ is $\S{t,v}$. This configuration has a valuation given by
$v(\S{t,v}) = p(t)\cdot p(v)$. Therefore, we conclude that
$v(\S{t}) > v(\S{t,v})$, meaning that the conservation condition is not
satisfied.

In the asymmetric case, we note that $p(t)=p(u)=p(v)=1$, since all cells consist
of a single event. Furthermore, the covering of the configuration $\S{t}$ is
$\S{\S{t,u}, \S{t,v}}$. The sum of the valuations for the configurations in this
covering is then
\[ v(\S{t,u}) + v(\S{t,v}) = p(t)\cdot p(u) + p(t)\cdot p(v) = 2, \]
which violates the conservation condition since
$v(\S{t,u}) + v(\S{t,v}) > v(\S{t})$.

\begin{figure}[htbp]
  \begin{subfigure}[b]{.47\linewidth}
    \[
      \xymatrix{
        \event \ar@{~}[r]\nameup{t}
        & \event \ar@{~}[r]\nameup{u}
        \POS[]-<1.5pc,0pc> *+=<3.75pc,1pc>[F--]{}
        & \event\nameup{v}
        \POS[]-<1.5pc,0pc> *+=<3.75pc,0.7pc>[F--]{}
      }
    \]
    \caption{Symmetric confusion.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{.47\linewidth}
    \[
      \xymatrix{
        \event \ar[d]\nameleft{t}
        \POS[]-<0pc,0pc> *+=<1pc,1pc>[F--]{} & \\
        \event \ar@{~}[r]\nameleft{u}
        \POS[]-<0pc,0pc> *+=<1pc,1pc>[F--]{}
        & \event\nameright{v}
        \POS[]-<0pc,0pc> *+=<1pc,1pc>[F--]{}
      }
    \]
    \caption{Asymmetric confusion.}
  \end{subfigure}
  \caption{Cells of event structures with confusion}%
  \label{fig:cellsconfusion}
\end{figure}

We conclude that the method by~\cite{Varacca} does not work for nets with
confusion because the cells are not closed under immediate conflict. To remedy
this, a new type of cells needs to be defined that are closed under immediate
conflict when the net has confusion. This approach is discussed in the next
chapter.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Capstone/capstone"
%%% TeX-command-extra-options: "--shell-escape"
%%% End:
