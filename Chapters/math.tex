\chapter{Preliminaries}%
\label{chap:preliminaries}
In this chapter we will introduce the mathematical notions that are used
throughout this paper.

\section{Relations, Orders and Graphs}
For binary relations, we introduce the transitive closure in the standard way.

\begin{defn}[Transitive closure]
  Let $F\subseteq A\times A$ be a binary relation on $A$. We define the
  \emph{irreflexive transitive closure} $F^+$as
  \begin{align*}
    F_0 &\deq F \\
    F_{n+1} &\deq F\union\S{(x,y) \given \Exists{z} xF_nzF_ny}\\
    F^+ &= \Union_{n\in\NN} F_n.
  \end{align*}
  We then define the \emph{(reflexive) transitive closure} $F^*$ as
  \[ F^* \deq F^+ \union \Id \quad\text{where}\quad \Id \deq \S{(x,x) \given x\in A}. \]
\end{defn}

This paper uses \emph{partial orders} extensively, requiring the following
notions and theorems.

\begin{defn}[Partial order]
  A pair $(A,\leq)$ with a set $A$ and a relation $\leq\,\subseteq A\times A$ is
  called a partial order if for all $a,b,c\in A$:
  \begin{itemize}
    \item (Reflexivity) $a\leq a$;
    \item (Antisymmetry) $a\leq b \wedge b\leq a \implies a=b$;
    \item (Transitivity) $a\leq b \wedge b\leq c \implies a\leq c$.
  \end{itemize}
\end{defn}

\begin{defn}[Maximal element]
  Let $(A,\leq)$ be a partial order. We say that an element $a\in A$ is
  \emph{maximal} in $A$ if
  \[ \Forall{x\in A} a\leq x \implies a=x. \]
  Given a finite subset $X\subseteq A$, we let $\max(X)$ denote the set of
  maximal elements of $(X,\leq)$.
\end{defn}

\begin{prop}%
  \label{prop:equal_iff_max_equal}
  Let $U$ and $V$ be finite downwards closed subsets of the partial order
  $\T{A,\leq}$. Then we have that
  \[ U = V \quad\text{iff}\quad \max(U) = \max(V). \]
\end{prop}

\begin{proof}
  Trivially, we have that if $U=V$ then $\max(U)=\max(V)$.
 
  For the other direction we assume that $\max(U)=\max(V)$. Without loss of
  generality, assume $x\in U$. We will prove that $x\in V$. There are 2
  cases, either $x\in\max(U)$ or $x\notin\max(U)$. If $x\in\max(U)$ then
  $x\in\max(V)$ and $x\in V$. Now, if $x\notin\max(U)$ then there is a
  $y\in\max(U)$ such that $x\leq y$ by finiteness of $U$. Therefore $y\in V$ and
  $x\in V$ since $V$ is downwards closed. Therefore, we conclude that
  \[ \max(U) = \max(V) \implies U = V. \qedhere\]
\end{proof}

\begin{thm}[Zorn's Lemma]
  Let $(A,\leq)$ be a partial order. Assume that every totally ordered subset of
  $A$ has an upper bound. Then the set $A$ contains at least one maximal element.
\end{thm}

The definition of Petri nets canonically uses a \emph{multiset} (also known as a
\emph{bag}) to represent a set that can contain an element multiple times.

\begin{defn}[Multiset]
  Call a multiset a mapping $m\,:\,A\to\NN$ for some set $A$. We define the
  elementary binary operations with $m$ and $n$ multisets on the set $A$:
  \begin{align*}
    m + n &\deq \S{\T{a,m(a)+n(a)} \given a\in A},\\
    m\diff n &\deq \S{\T{a,\max\T{m(a)-n(a),0}} \given a\in A}.
  \end{align*}
  Since multisets behave much like sets, we additionally define the standard set
  operations such that multisets behave like regular sets if
  $\Forall{a\in A} m(a)\leq 1\wedge n(a)\leq 1$:
  \begin{align*}
    m\union n &\deq m+(n\diff m),\\
    m\intersect n &\deq m\diff(n\diff m).
  \end{align*}
  The membership, subset and proper subset relations are then defined as:
  \begin{align*}
    a\in m &\iff m(a) > 0,\\
    m\subseteq n &\iff m\intersect n=m,\\
    m\subset n &\iff m\subseteq n \wedge n\neq m.
  \end{align*}
  To be able to apply the operations above with a multiset and a normal set $X$
  as operands, we map $X$ to a multiset given by
  \[ \S{(a,1) \given a\in X} \union \S{(a,0) \given a\in A\diff X}. \]
  Additionally, we represent multisets as sets possibly containing the identical
  elements multiple times.

  The definitions above are extended to the case where a multiset $m$ can
  contain an element infinitely many times, in which case
  $m\,:\,A\to\NN_\infty$, where $\NN_\infty$ is the set of natural numbers
  including infinity.
\end{defn}

Sometimes, we will look at the graph structures of Petri nets. Therefore, we
introduce the following notions.

\begin{defn}[Directed graph]
  A directed graph is a pair $\T{\mathcal{N},\mathcal{E}}$ where $\mathcal{N}$
  is the set of \emph{nodes} and
  $\mathcal{E}\subseteq\mathcal{N}\times\mathcal{N}$ is the set of \emph{edges}.
\end{defn}

\begin{defn}[Walk, path \& cycle]
  A sequence $\T{x_1,\dots,x_n}$ of nodes in a directed graph
  $G=\T{\mathcal{N},\mathcal{E}}$ is called a \emph{walk} if
  $\Forall{0<i<n} \T{x_i,x_{i+1}}\in\mathcal{E}$. A walk is called a
  \emph{path} if all nodes in the walk are distinct. A walk is called a
  \emph{cycle} if $\T{x_1,\dots,x_{n-1}}$ is a path and $x_1=x_n$.
\end{defn}

\begin{defn}[Strongly connected component]
  Given a directed graph $G=\T{\mathcal{N},\mathcal{E}}$, we call a \emph{strongly
  connected component} a maximal set of nodes $\S{x_i}\subseteq\mathcal{N}$ such
  that there exists a path between any ordered pair of nodes $(x_i, x_j)$ with
  $i\neq j$.
\end{defn}

\section{Probability}%
\label{sec:probability}
The definitions of probability in this paper are based on the canonical
measure-theory approach, following the example set by~\cite{AB_markov}.

\begin{defn}[Measurable space \& $\sigma$-algebra]
  Let $U$ be a non-empty set. A set $\mathfrak{F}$ of subsets of $U$ is
  called a \emph{$\sigma$-algebra} if $\emptyset\in\mathfrak{F}$ and
  $\mathfrak{F}$ is closed under complement and countable union. The pair
  $\T{U,\mathfrak{F}}$ is then called a \emph{measurable space}. The elements of
  $\mathfrak{F}$ are called \emph{measurable subsets} of $\T{U,\mathfrak{F}}$.
\end{defn}

\begin{defn}[Measurable mapping]
  If $\T{U,\mathfrak{F}}$ and $\T{V,\mathfrak{G}}$ are measurable spaces, then
  a mapping $\phi\,:\,U\to V$ is called a \emph{measurable mapping} if
  $\inv\phi(A)\in\mathfrak{F}$ for every $A\in\mathfrak{G}$. Adopting the
  terminology from probability theory, measurable mappings are also called
  \emph{random variables}.
\end{defn}

\begin{defn}[Induced $\sigma$-algebra]
  Let $(U,\mathfrak{F})$ be a measurable space. For any measurable subset $A$ of
  $U$ there is a measurable space $\T{A,\mathfrak{F}^A}$, where we say that
  $\mathfrak{F}^A$ is \emph{induced} by $\mathfrak{F}$ and is defined by
  \[ \mathfrak{F}^A \deq \S{B\in\mathfrak{F}\given B\subseteq A}. \]
\end{defn}

\begin{defn}[Probability space \& probability measure]
  If $\T{U,\mathfrak{F}}$ is a measurable space, then the triple
  $\T{U,\mathfrak{F},\Prob}$ is said to be a \emph{probability space} if
  $\Prob$ is a non-negative set function
  $\Prob\,:\,\mathfrak{F}\to\RR$ such that $\Prob(\emptyset)=0$ and
  $\Prob(U)=1$ and for any sequence $\T{A_n}_{n\geq 0}$ of pairwise
  disjoint measurable subsets, we have
  \[\Prob\T{\Union_{n\geq 0}A_n}=\sum_{n\geq 0}\Prob\T{A_n}. \]
  The map $\Prob$ is then called a \emph{probability measure} or simply a
  \emph{probability} and can be determined by how it acts
  on the singletons $\S{x}$ with $x\in U$. Therefore, we write
  $\Prob\T{x} = \Prob\T{\S{x}}$, yielding $\sum_{x\in U}\Prob\T{x} = 1$.
\end{defn}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Capstone/capstone"
%%% TeX-command-extra-options: "--shell-escape"
%%% End:
