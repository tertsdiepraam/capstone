\chapter{Concurrency}%
\label{chap:concurrency}
In this chapter we will present the classes of models for concurrent systems.
Additionally, we treat the application of concurrency in the field of physics.

\section{Models of Concurrency}
There are multiple models of concurrency, each highlighting different
characteristics of concurrent systems. Most of these models take as their atomic
element some indivisible (sequential) change (usually called events, actions,
transitions or symbols)~\cite{Sassone}. These changes are then composed into
more complex concurrent structures by specifying an order of causality between
them. This order of causality in a concurrent system is fundamentally a partial
order~\cite{Lamport}. If this partial order is used, we say that
\emph{true-concurrent} or \emph{non-interleaving} semantics are used.
Alternatively, the events can be totally ordered. In this case we call the
semantics \emph{interleaving}.

\textcite{Sassone} have introduced three axes along which to compare models of
concurrency. The first axis distinguishes \emph{system} and
\emph{behaviour} models. System models have a representation of the state of a
system, while behaviour models are not concerned with state itself, but only
with the patterns of states. The second axis describes whether the system uses
true-concurrent or interleaving semantics. Finally, the third axis distinguishes
\emph{branching time} models from \emph{linear time} models. Branching models
have a representation of the choices in the system, while linear models do not.

In this paper, Petri nets and event structures will be used as models for
concurrency. In the taxonomy from~\cite{Sassone}, Petri nets are an example of a
system non-interleaving branching time model~\cite{Sassone}. Similarly, event
structures are a behaviour non-interleaving branching time model~\cite{Sassone}.
Although both can also be equipped with interleaving semantics.

\section{Concurrency in Physics}
We end the chapter by briefly considering some connections between concurrency
theory and physics.

The necessity of true-concurrency does not only arise for practical
synchronisation reasons, but true-concurrency is in fact a fundamental property
of the universe that follows from the theory of special relativity. The theory
of special relativity is based on two main postulates~\cite{Einstein}:

\begin{enumerate}
  \item the laws of physics are of the same form in all inertial reference
    frames\footnote{Inertial means non-accelerating.};
  \item the speed of light in vacuum has the same value $c$ in all inertial
    reference frames.
\end{enumerate}

The constant speed of light implies that vector addition cannot be used to add
velocities. Instead, \textcite{Einstein} proposed to use Lorentz
transformations. The remarkable property of Lorentz transformations with respect
to concurrency is that it introduces \emph{time dilation}, effectively
stretching time depending on the velocity of the observer.

Consider two events separated by space $a$ and $b$, which are simultaneous in
one observer's frame. If a second observer is moving towards $a$, the Lorentz
transformation results in $a$ happening before $b$ from the perspective of this
observer, in which case they are no longer simultaneous.\footnote{It must be
noted that causality cannot be affected by Lorentz transformations.} It follows
from the first postulate that there exists no absolute reference frame that can
be regarded as the sole truth. Therefore, these events are fundamentally
true-concurrent.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Capstone/capstone"
%%% TeX-command-extra-options: "--shell-escape"
%%% End:

% LocalWords: probabilistically
