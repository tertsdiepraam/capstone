\chapter{Petri Nets}%
\label{chap:petri}
In this section, the basic theory of Petri nets will be introduced. Then, the
concepts of confusion and of persistent places will be discussed. These notions
will have a crucial role in \cref{chap:scells,chap:branchingcells}. Unless
otherwise specified, this chapter follows the definitions and notation
from~\cite{Montanari}.

\section{Preliminaries on Petri Nets}
Petri nets were first introduced by C.\,A. Petri in
\citeyear{Petri}~\cite{Petri} to model concurrent processes such as chemical
processes. Since then, Petri nets have been generalised to describe any kind of
concurrent process.

\begin{defn}[Petri net]
  \nompet{$\pre p$}{preset}
  \nompet{$\post p$}{postset}
  A Petri net is a tuple $\T{ P,T,F }$ where
  \begin{itemize}
    \item $P$ is the set of places,
    \item $T$ is the set of transitions, which is disjoint from $P$,
    \item $F\subseteq (P\times T)\cup(T\times P)$ is the flow relation.
  \end{itemize}
 
  A \emph{marked Petri net} is a tuple $\T{ P,T,F,m }$, where $\T{P,T,F}$ is a
  Petri net and $m$ is a multiset $m\in\NN^P$, that is, $m$ is a mapping from
  $P$ to $\NN$. If $p\in m$, then $p$ is said to be \emph{marked} at $m$.

  Adopting terminology from graph theory, the set $P\union T$ is called the set
  of \emph{nodes}. If $x$ is a node of $N$ then we write $x\in N$. Given a node
  $x\in N$, we define the \emph{preset} $\pre x$ and \emph{postset} $\post x$ of
  $x$ as
  \[
    \pre x \deq \S{ y \given \T{y,x} \in F }
    \quad\text{and}\quad
    \post x \deq \S{ z \given \T{x,z} \in F }.
  \]
  Additionally, we define the \emph{initial places} $\init N$ and the
  \emph{final places} $\final N$ of the Petri net $N$ as
  \[
    \init N \deq \S{p\in P \given \pre p = \emptyset}
    \quad\text{and}\quad
    \final N \deq \S{p\in P \given \post p = \emptyset}.
  \]

  A Petri net $N'=(P',T',F')$ is called a subnet of a Petri net $N=(P,T,F)$,
  written $N'\subseteq N$ if
  $P'\subseteq P\wedge T'\subseteq T\wedge F'\subseteq F$.
\end{defn}

Furthermore, we define the \emph{causality relation} as the
transitive closure of the flow relation:
\nompet{$\preceq$}{causality relation}
\[ \preceq\,\deq F^*. \]

To remove nodes from a Petri net, we define the set difference on a Petri net
$N=(P,T,F)$ as\nompet{$\diff$}{removal of nodes}
\[ N\diff X = \T{P', T', F'}, \]
with
\[ P' = P\diff X,\quad T' = T\diff X,\quad F' = F\intersect\T{\T{P'\times T'}\union\T{T'\times P'}}. \]

All operations on Petri nets in this paper are implicitly extended to marked
Petri nets, restricting $m$ to the elements of $P$ if marked places are removed.

In modelling a concurrent process with a Petri net, one associates a place with
each condition that might hold during the execution process. The state at a
given time in the process is given by the set of places in the marking. The
transitions then represent the ability of the system to change its state from
one marking to another.

\begin{defn}[Enabling \& firing]
  A transition $t$ is \emph{enabled} at the marking $m$, written $\fire{m, t}$
  if $\pre t \subseteq m$. If a transition is \emph{fired}, written $\fire{m, t, m'}$,
  then $m' = (m\diff \pre t) + \post t$. A finite sequence of firings is
  called a \emph{firing sequence}. A firing sequence
  $\fire{m_0, t_1, \dots, t_n, m_n}$ is abbreviated
  $\fire{m_0, t_1\cdots t_n, m_n}$ or $\fire{m, *, m'}$. Given an initial
  marking, firing sequences can be uniquely identified by a sequence of
  transitions.
\end{defn}

As a result of these definitions, places can hold any (natural) number of
tokens. A marked Petri net is said to be \emph{$1$-safe} if it is impossible to
reach a marking via a firing sequence where a single place holds two or more
tokens. In this paper, only $1$-safe Petri nets are considered.

Let us illustrate the use of Petri nets with a simple example. Consider a shop
with a single shopkeeper $s$ and two customers, labelled $a$ and $b$. Before
they can buy something, the customers have to decide what to buy. When they are
ready to order, they complete the transaction with the shopkeeper, after which
the customers leave the shop.

In this model, each customer has 3 possible states: ``\verb|thinking|'' (about
what to buy), ``\verb|ready|'' (to order) and ``\verb|left|'' (the shop). The
shopkeeper can be either ``\verb|free|'' or ``\verb|busy|'' helping a customer.
Initially, each customer is in the ``\verb|thinking|'' state and the shopkeeper
is in the ``\verb|free|'' state. When the customer decide what they want, they
transition from ``\verb|thinking|'' to ``\verb|ready|''. When the shopkeeper and
a customer are ``\verb|free|'' and ``\verb|ready|'', respectively, they
transition to ``\verb|busy|''. After an arbitrary amount of time, the
transaction is completed and the customer's state is set to ``\verb|done|'' and
the shopkeeper returns to being ``\verb|free|''.

The marked Petri net corresponding to this process is shown in \cref{fig:shop}. In
particular, \cref{fig:shop1} shows the initial state of the process. \Cref{fig:shop2,fig:shop3,fig:shop4} then show the evolution of
the markings when customer $a$ makes a decision, is helped by the shopkeeper
$s$, and leaves the shop. Places are represented by circles, transitions are
represented with rectangles and the flow relation is represented by edges
between the nodes. Places contain a number of dots corresponding to their
marking.

\begin{figure}[htbp]
  \begin{subfigure}{\linewidth}
    \centering
    \begin{tikzpicture}[x=1.5cm]
      \p[left:$a$ thinking]{a1}{0,6}
      \t[decide]{a2}{0,5}
      \p[left:$a$ ready]{a3}{0,4}
      \p[left:$a$ left]{a4}{0,0}

      \p[right:$b$ thinking]{b1}{4,6}
      \t[decide]{b2}{4,5}
      \p[right:$b$ ready]{b3}{4,4}
      \p[right:$b$ left]{b4}{4,0}

      \p[$s$ free]{s}{2,4}

      \t[begin]{sa1}{1,3}
      \p[left:$s$ helping $a$]{sa2}{1,2}
      \t[done]{sa3}{1,1}

      \t[begin]{sb1}{3,3}
      \p[right:$s$ helping $b$]{sb2}{3,2}
      \t[done]{sb3}{3,1}

      \flow{
        a1/a2, a2/a3, a3/sa1, sa1/sa2, sa2/sa3, sa3/a4,
        b1/b2, b2/b3, b3/sb1, sb1/sb2, sb2/sb3, sb3/b4,
        s/sa1, s/sb1}

      \marking{a1, s, b1}

      \begin{pgfonlayer}{background}
        \draw (sa3) edge[post, out=300, in=-90] (s);
        \draw (sb3) edge[post, out=-120, in=-90] (s);
      \end{pgfonlayer}
    \end{tikzpicture}
    \caption{The net with the initial marking and descriptions for the places
      and transitions.}%
    \label{fig:shop1}
  \end{subfigure}
 
  % Figure 2
  \begin{subfigure}[t]{0.3\linewidth}
    \centering
    \begin{tikzpicture}
      \shopnet
      \marking{a3, s, b1}
    \end{tikzpicture}
    \caption{The net after firing $t_1$.}%
    \label{fig:shop2}
  \end{subfigure}
  \hfill
  % Figure 3
  \begin{subfigure}[t]{0.3\linewidth}
    \centering
    \begin{tikzpicture}
      \shopnet
      \marking{sa2, b1}
    \end{tikzpicture}
    \caption{The net after firing $t_1$ and $t_3$.}%
    \label{fig:shop3}
  \end{subfigure}
  \hfill
  % Figure 4
  \begin{subfigure}[t]{0.3\linewidth}
    \centering
    \begin{tikzpicture}
      \shopnet
      \marking{a4, s, b1}
    \end{tikzpicture}
    \caption{The net after firing $t_1$, $t_3$ and $t_4$.}%
    \label{fig:shop4}
  \end{subfigure}
  \caption{The Petri net corresponding to the shop example. The net starts
    out with the initial marking in \cref{fig:shop1}.
    \Cref{fig:shop2,fig:shop3,fig:shop4} show the
    dynamics of the system, after the transitions $t_1$, $t_3$ and $t_4$ are
    fired, respectively.}%
  \label{fig:shop}
\end{figure}

As defined above, a transition can only be fired when all the places in its
preset are marked. When a transition is fired, the places in the preset are
removed from the marking and the places in the postset are added. If transitions
share one or more places in their preset, a choice between the transitions has
to made. As we will see, choices will play a central role in this paper.

\begin{defn}[Conflict]%
  \label{defn:conflict}
  \nompet{$\dconflict$}{direct conflict relation}
  \nompet{$\conflict$}{conflict relation}
  Two transitions, $t_1$ and $t_2$, are in \emph{direct conflict}, written
  $t_1\dconflict t_2$, if
  \[ t_1 \neq t_2 \wedge \pre{t_1} \cap \pre{t_2} \neq \emptyset. \]
  Additionally, two nodes $x, y\in N$ are in \emph{conflict}, denoted
  by $x\conflict y$, if any of their ``ancestors'' are conflict. Formally, $\conflict$ is
  defined by
  \[
    \Forall{x,y\in N}\quad x\conflict y \text{ iff }
    \Exists{e\preceq x}\Exists{e'\preceq y}e\dconflict e'.
  \]
\end{defn}

It is possible that a node $x$ has nodes in its preset which are in conflict with
each other. By \cref{defn:conflict}, this implies $x\conflict x$. In this
case, we say that $x$ is in conflict with itself and that there is
\emph{auto-conflict} in the net.

We define the previously discussed concept of confusion in the context of
Petri nets.

\begin{defn}[Confusion]%
  \label{defn:confusion}
  A $1$-safe marked net has confusion if there exists a reachable marking
  $m$ and transitions $t$, $u$, $v$ such that the symmetric or asymmetric
  case holds.
  \begin{enumerate}
    \item In the symmetric case,
      \begin{enumerate}
        \item $t, u, v$ are enabled at $m$,
        \item $\pre t \intersect \pre u \neq \emptyset \neq \pre u \intersect \pre v$ and
        \item $\pre t \intersect \pre v = \emptyset$.
      \end{enumerate}
    \item In the asymmetric case,
      \begin{enumerate}
        \item $t$ and $v$ are enabled at $m$,
        \item $u$ is not enabled at $m$ but becomes enabled after the firing
          of $t$,
        \item $\pre t \intersect \pre v = \emptyset$ and
          $\pre u \intersect \pre v \neq \emptyset$.
      \end{enumerate}
  \end{enumerate}
  When a net does not have confusion, it is said to be \emph{confusion-free}.
\end{defn}

Consider the net in \cref{fig:sconf}. If $t$ is fired, $u$ is no longer enabled,
removing the choice between $u$ and $v$. This net has symmetric confusion.
Similarly, if $t$ is fired in the net in \cref{fig:aconf}, $u$ becomes enabled
and a choice between $u$ and $v$ is added. This is an example of asymmetric
confusion.

\begin{figure}
  % Symmetric confusion
  \begin{subfigure}[b]{.47\textwidth}
    \centering
    \begin{tikzpicture}
      \sconf
    \end{tikzpicture}
    \caption{Symmetric confusion.}%
    \label{fig:sconf}
  \end{subfigure}
  \hfill
  % Asymmetric confusion
  \begin{subfigure}[b]{.47\textwidth}
    \centering
    \begin{tikzpicture}
      \aconf
    \end{tikzpicture}
    \caption{Asymmetric confusion.}%
    \label{fig:aconf}
  \end{subfigure}
  \caption{The two simple cases of confusion.}%
  \label{fig:basicconf}
\end{figure}

\section{Occurrence Nets}
An occurrence net is an acyclic Petri net without auto-conflict. They will be
instrumental in connecting Petri nets to event structures in \cref{chap:event}.

\begin{defn}[Occurrence net]
  A Petri net $N=\T{ P, T, F }$ is called an \emph{occurrence net} if it satisfies
  the following properties:
  \begin{itemize}
    \item $\T{ P\union T, \preceq }$ is a partial order;
    \item for every $x\in P\union T$, the set
      $\S{y\in P\union T \given y\preceq x}$ is finite;
    \item for every $p\in P$, $\abs{\pre p}\leq 1$; and
    \item $\conflict$ is irreflexive, meaning that there is no auto-conflict.
  \end{itemize}
\end{defn}

The definition above is illustrated in \cref{fig:nonocc,fig:occ}.
\Cref{fig:nonocc} shows nets that are not occurrence nets, since they are not
acyclic, have places with a preset of size larger than $1$ or contain nodes
which are in conflict with themselves. In contrast, \cref{fig:occ} shows
examples of nets that are occurrence nets.

\begin{figure}[htbp]
  \begin{subfigure}[t]{.3\linewidth}
    \centering
    \trimbox{.8cm .9cm 0 0}{
      \begin{tikzpicture}
        \p{a}{0,2}
        \t{b}{0,1}

        \flow{a/b}

        \begin{pgfonlayer}{background}
          \draw[post] (b) .. controls +(down:2cm) and +(left:1.3cm) .. (a);
        \end{pgfonlayer}
      \end{tikzpicture}
    }
    \caption{A net with a cycle.}%
    \label{fig:nonocc1}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.3\linewidth}
    \centering
    \begin{tikzpicture}
      \p{a}{1,2}
      \t{1}{0,1}
      \t{2}{2,1}
      \p{b}{1,0}

      \flow{a/1, a/2, 1/b, 2/b}
    \end{tikzpicture}
    \caption{A net with a place such that $\abs{\pre p} > 1$.}%
    \label{fig:nonocc2}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.3\linewidth}
    \centering
    \begin{tikzpicture}
      \p{a}{1,4}
      \t{1}{0,3}
      \t{2}{2,3}
      \p{b}{0,2}
      \p{c}{2,2}
      \t{3}{1,1}
      \p{d}{1,0}

      \flow{a/1, a/2, 1/b, 2/c, b/3, c/3, 3/d}
    \end{tikzpicture}
    \caption{A net with auto-conflict.}
  \end{subfigure}
  \caption{Nets that are \emph{not} occurrence nets since they each do not
    satisfy one of the necessary properties. Adapted from~\cite{Abbes}.}%
  \label{fig:nonocc}
  \begin{subfigure}[t]{.47\linewidth}
    \centering
    \begin{tikzpicture}
      \p{a}{1,2}
      \t{1}{0,1}
      \t{2}{2,1}
      \p{b}{0,0}
      \p{c}{2,0}

      \flow{a/1, 1/b, a/2, 2/c}
    \end{tikzpicture}
    \caption{A tree-like net is an occurrence net.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.47\linewidth}
    \centering
    \begin{tikzpicture}
      \p{a}{1,4}
      \t{1}{1,3}
      \p{b}{0,2}
      \p{c}{2,2}
      \t{2}{1,1}
      \p{d}{1,0}

      \flow{a/1, 1/b, 1/c, b/2, c/2, 2/d}
    \end{tikzpicture}
    \caption{Concurrent processes are possible as long as conflicting processes
      are not joined.}
  \end{subfigure}
  \caption{Examples of occurrence nets. Adapted from~\cite{Abbes}.}%
  \label{fig:occ}
\end{figure}

To identify the interleaving traces of Petri nets, we introduce equivalence
classes called \emph{processes}.

\begin{defn}[Deterministic occurrence net]
  An occurrence net is called \emph{deterministic} if there is no
  conflict.
\end{defn}

\begin{defn}[Process]%
  \label{defn:process}
  A \emph{deterministic nonsequential process} or process is a mapping
  $\pi: \mathcal{D}\to N$ from a deterministic occurrence net $\mathcal{D}$ to a
  Petri net $N$ that preserves preset and postset and such that
  $\pi\T{\initcell{\mathcal{D}}}$ is the initial marking of $N$. Firing sequences in $N$
  are equivalent if they are images of maximal firing sequences in the same
  deterministic occurrence net $\mathcal{D}$. A process is called \emph{maximal}
  if its maximal firing sequences are maximal in $N$.
\end{defn}

For example in the net in \cref{fig:shop}, the firing sequences
\begin{gather*}
  \T{ t_2, t_1, t_3, t_4 }, \\
  \T{ t_1, t_2, t_3, t_4 }, \\
  \T{ t_1, t_3, t_2, t_4 }, \\
  \T{ t_1, t_3, t_4, t_2 },
\end{gather*}
are all considered to be equivalent and belong to the equivalence class given by
the deterministic occurrence net $U=\T{ P', T', F' }$ with
\begin{align*}
  T' &= \S{t_1, t_2, t_3, t_4}, \\
  P' &= \Union_{t\in T'} (\pre t \union \post t), \\
  F' &= F \intersect \T{ \T{P'\times T'} \union \T{P'\times T'} }.
\end{align*}

\section{Unfolding}
A $1$-safe marked Petri net can be \emph{unfolded} into an occurrence net while
preserving the possible firing sequences. This occurrence net can be used as a
canonical representation of the dynamics of the net; when two $1$-safe nets have
the same behaviour, their occurrence nets will be
equal~\cite[Thm.~4.7]{Winskel_unfolding}. Unfolding is also the first step in
converting a Petri net into an event structure, as will be discussed in
\cref{sec:petri_to_event}. The construction of the unfolding detailed below is based
on~\cite{Nielsen}.

The unfolding is constructed using an equivalence relation $\equiv$ on firing
sequences. To define $\equiv$, we take any firing sequence
\[ \sigma = \fire{m_0, t_0\cdots t_n, m_{n+1}}, \]
where $m_0$ is the initial marking. The equivalence relation $\equiv$ will be
defined from two auxiliary relations. First, we say that $\sigma\equiv_{(1)}\sigma'$
if there is
$i\leq n-1$ such that
\[ \sigma' = \fire{m_0, t_0\cdots t_{i-2}, m_{i-1}, t_i, m'_i, t_{i-1}, m_{i+1}, t_{i+1}\cdots t_n, m_{n+1}}, \]
that is, the positions of $t_i$ and $t_{i-1}$ are swapped, changing the marking
$m_i$ to some other marking $m'_i$. Second, we say that
$\sigma\equiv_{(2)}\sigma''$ if
\[ \sigma'' = \fire{m_0, t_0\cdots t_{n-2}, m_{n-1}, t_n, m'_{n+1}}, \]
where $t_{n-1}$ is removed from the firing sequence, changing the marking
$m_{n+1}$ to some other marking $m'_{n+1}$. We then define $\equiv$ as the
reflexive symmetrical transitive closure of $\equiv_{(1)}$ and $\equiv_{(2)}$:
\[
  \equiv\ \deq \T{
    \equiv_{(1)}
    \union\equiv_{(2)}\union\equiv^{\invsymb}_{(2)}
  }^*.
\]

We denote the set of equivalence classes of $\equiv$ with $S$. Note that all
firing sequences in an equivalence class $s\in S$ of $\equiv$ have the same final
transition which we identify as $t_s$. Additionally, given firing sequences
\begin{align*}
  \sigma &= \fire{m_{0}, t_0\cdots t_{n-1}, m_n, t_n\cdots t_{k-1}, m_k, t_k\cdots t_{l-1}, m_l},\\
  \sigma' &= \fire{m_n, t_n\cdots t_{k-1}, m_k},\\
  \intertext{abusing notation, we freely write $\sigma$ as}
  \sigma &= \fire{m_0, t_0\cdots t_{n-1}, \sigma', t_k\cdots t_{l-1}, m_l}.
\end{align*}

The unfolding of a $1$-safe Petri net $(P, T, F)$ is then an occurrence net
$U=\T{P', T', F'}$, where
\begin{align*}
  T' &= S,\\
  P' &= \S{\T{e,p} \given s\in S \wedge p\in\post t_s} \union \S{\T{\emptyset, p} \given p\in m },\\
  F' &= \begin{aligned}[t]
         &\S{\T{s,\T{s,p'}} \given \T{s,p'} \in P'}\\
       &\union \S{\T{p',s} \given
         p' = \T{s', p}
         \wedge s=s'\fire{, t_n, m_{n+1}}
         \wedge p\in\pre t_s}\\
       &\union \S{\T{p', s} \given
         p' = \T{\emptyset, p}
         \wedge s=\fire{m_0, t_0, m_1}
         \wedge p\in\pre t_0}.
       \end{aligned}
\end{align*}

A simple Petri net and its unfolding are illustrated in \cref{fig:unfolding},
Only a prefix of the unfolding is shown since $t_1$ can be fired any number of
times in the original net.

\begin{figure}[htbp]
  \begin{subfigure}[t]{.47\linewidth}
    \centering
    \begin{tikzpicture}
      \p[above:$a$]{a}{1,0}
      \p[above:$b$]{b}{3,0}
      \t[$t_1$]{1}{0,-1}
      \t[$t_2$]{2}{2,-1}
      \t[$t_3$]{3}{4,-1}
      \p[below:$c$]{c}{2,-2}
      \p[below:$d$]{d}{4,-2}

      \flow{a/1, a/2, b/2, b/3, 2/c, 3/d}

      \begin{pgfonlayer}{background}
        \draw[post] (1) .. controls +(down:1cm) and +(down:2cm) .. (a);
      \end{pgfonlayer}

      \marking{a,b}
    \end{tikzpicture}
    \caption{A net containing a loop.}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.47\linewidth}
    \centering
    \begin{tikzpicture}
      \p[above:$a$]{a1}{1,0}
      \p[above:$b$]{b}{3,0}
      \t[$t_1$]{11}{0,-1}
      \t[$t_2$]{21}{2,-1}
      \t[$t_3$]{3}{4,-1}
      \p[left:$a$]{a2}{0,-2}
      \p[left:$c$]{c1}{2,-2}
      \p[below:$d$]{d}{4,-2}
      \t[$t_1$]{12}{0,-3}
      \t[$t_2$]{22}{2,-3}
      \p[left:$a$]{a3}{0,-4}
      \p[left:$c$]{c2}{2,-4}
      \node (d1) at (0,-5) {$\vdots$};
      \node (d2) at (2,-5) {$\vdots$};

      \flow{
        a1/11, a1/21, b/21, b/3,
        11/a2, 21/c1, 3/d,
        a2/12, a2/22,
        12/a3, 22/c2,
        a3/d1, c2/d2%
      }

      \marking{a1,b}

      \begin{pgfonlayer}{background}
        \draw[post] (b) -- ([xshift=.3cm]22.north);
        \draw[post] (b) -- ([xshift=.4cm]d2.north);
      \end{pgfonlayer}

    \end{tikzpicture}
    \caption{The prefix of the unfolding of the net.}
  \end{subfigure}
  \caption{A cyclic net and its unfolding. Adapted from~\cite{Abbes}.}%
  \label{fig:unfolding}
\end{figure}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Capstone/capstone"
%%% TeX-command-extra-options: "--shell-escape"
%%% End:

% LocalWords:  acyclic nonsequential
