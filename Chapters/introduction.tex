\chapter{Introduction}

Concurrency theory gained popularity due to the rise of parallelisation in
computing and long-distance communication methods. A process is called
\emph{concurrent} if the order of some events can be changed without affecting
the final state of the process. Since concurrent events can often be executed in
parallel, concurrency is often used to optimise computer programs. However, the
concept of concurrency applies to more than just computer science. In fact,
concurrency is not an exceptional property; many processes such as chemical
reactions and traffic are concurrent.

Models of concurrency can be distinguished based on whether they use
\emph{interleaving} or \emph{true-concurrent} semantics. In a true-concurrent
model, the events are only partially ordered. In an interleaving model,
the events are totally ordered. In this paper, we focus on two models of
concurrency: Petri nets and event structures.

Probability is often used in models for concurrent processes to make the choices
probabilistic which is, for example, useful for simulation and performance
analysis of a process. Active research is being done to investigate how
probabilistic choices can be added to Petri nets. Ideally, such a model
satisfies the following conditions:

\begin{enumerate}
  \item the behaviour of the probabilistic model must match the behaviour of
    standard Petri nets;
  \item equivalent processes must have equal probabilities;
  \item the sum of probabilities of all maximal processes should be $1$; and
  \item the probabilities of executing concurrent events should be independent.
\end{enumerate}

Satisfying the third condition is especially difficult, since it is possible
to construct a system where concurrent events are dependent on each other. When
this is the case, we say that a system has \emph{confusion}. Otherwise, we say
that it is \emph{confusion-free}.

\emph{Stochastic Petri nets} are a probabilistic model that uses interleaving
semantics~\cite{Bause}. However, this model significantly changes the behaviour
of Petri nets. Hence, this model violates the first condition
above~\cite[p.~6]{Bause}. A probabilistic model for concurrency using
true-concurrent semantics was introduced by \textcite{Varacca}. This model
satisfies all conditions above, but only applies to confusion-free Petri nets.
Two other methods for attaching probabilities to Petri nets with confusion using
true-concurrent semantics have been developed: the \emph{branching cells} method
was introduced by \textcite{AB_branching_cells} and the \emph{structural
branching cells} or \emph{s-cells} method by \textcite{Montanari}. The first is
a dynamic method that removes confusion by controlling the execution of the
Petri net. The second removes confusion by changing the Petri net statically to
a confusion-free net. However, the method by \textcite{Montanari} applies only
to acyclic Petri nets, while the method by \textcite{AB_branching_cells} applies
to locally finite Petri nets, a subclass of Petri nets which allows for
``controlled'' cycles.

In this paper, we explore the branching cells method and the s-cells method.
Additionally, we have implemented the s-cells method in a Python program, which
required further formalisation of the pruning step of the s-cells method.

This capstone is structured as follows: \Cref{chap:preliminaries} treats the
mathematical preliminaries in set theory and probability theory.
\Cref{chap:concurrency} details the classes of models for concurrent processes.
\Cref{chap:petri,chap:event} explain two of these models, Petri nets and event
structures, in depth. Finally,
\cref{chap:probability,chap:branchingcells,chap:scells} explore the methods for
attaching probabilities to these models by \textcite{Varacca},
\textcite{AB_branching_cells} and \textcite{Montanari}, respectively.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Capstone/capstone"
%%% TeX-command-extra-options: "--shell-escape"
%%% End:

%%% LocalWords: probabilistically acyclic
