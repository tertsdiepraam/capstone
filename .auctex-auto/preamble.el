(TeX-add-style-hook
 "preamble"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("hyperref" "hidelinks") ("biblatex" "maxcitenames=2" "backend=biber") ("subcaption" "position=bottom")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "../Nets/nets"
    "amsmath"
    "amssymb"
    "amsthm"
    "mathtools"
    "array"
    "tikz"
    "pgffor"
    "hyperref"
    "bookmark"
    "ifthen"
    "biblatex"
    "enumitem"
    "stmaryrd"
    "subcaption"
    "titling"
    "graphicx"
    "adjustbox"
    "setspace"
    "todonotes"
    "xparse")
   (TeX-add-symbols
    '("pb" ["argument"] 2)
    '("p" ["argument"] 2)
    '("SetSymbol" ["argument"] 0)
    '("marking" 1)
    '("flow" 1)
    '("fire" 1)
    '("final" 1)
    '("init" 1)
    '("post" 1)
    '("pre" 1)
    '("fref" 1)
    '("keywords" 1)
    '("email" 1)
    "maketitlepage"
    "HRule"
    "addref"
    "NN"
    "ZZ"
    "RR"
    "CC"
    "EE"
    "PP"
    "diff"
    "union"
    "intersect"
    "deq"
    "given")
   (LaTeX-add-environments
    "petrinet")
   (LaTeX-add-bibliographies
    "../bibliography")
   (LaTeX-add-amsthm-newtheorems
    "thm"
    "lemma"
    "prop"
    "defn")
   (LaTeX-add-mathtools-DeclarePairedDelimiters
    '("Set" "1")))
 :latex)

