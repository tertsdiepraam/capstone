\documentclass{article}

\input{../preamble}

\title{Removing Confusion in Locally Finite Petri Nets}
\author{Terts Diepraam}
\date{\today}

\begin{document}
\begin{titlepage}
    \newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
    
	\center
	\HRule\\[0.4cm]
	{\huge\bfseries \thetitle\\[0.4cm]}
	\HRule\\[1.5cm]
	
	\begin{minipage}{0.45\textwidth}
		\begin{flushleft}
			\large
			\textit{Author}\\
            Terts Diepraam\\
            {\small Amsterdam University College}\\
            {\small\email{terts.diepraam@gmail.com}}
		\end{flushleft}
	\end{minipage}
	~
	\begin{minipage}{0.4\textwidth}
		\begin{flushright}
			\large
			\textit{Supervisor}\\
            Dr. Lorenzo Galeotti\\
            {\small Amsterdam University College}\\
            {\small\email{lorenzo.galeotti@gmail.com}}
		\end{flushright}
    \end{minipage}
    \\[1cm]
    \begin{minipage}[t]{0.45\textwidth}
		\begin{flushleft}
			\large
			\textit{Tutor}\\
            Dr. Cor Zonneveld\\
            {\small Amsterdam University College}\\
            {\small\email{c.zonneveld@auc.nl}}
		\end{flushleft}
	\end{minipage}
	~
	\begin{minipage}[t]{0.4\textwidth}
		\begin{flushright}
			\large
			\textit{Reader}\\
            Dr. Yurii Khomskii\\
            {\small Amsterdam University College}\\
            {\small Institute for Logic, Language and Computation}\\
            {\small\email{yurii@deds.nl}}
		\end{flushright}
	\end{minipage}
    \vfill\vfill\vfill
	
    {\large
        Major: Sciences\\[0.5cm]
        \thedate
    }

    \vfill\vfill
    \includegraphics[width=0.2\textwidth]{{auc logo.png}}\\[1cm]
	
	\vfill
	
\end{titlepage}

\begin{abstract}
% a self-contained description of the activity to be undertaken, including: (a) overall project goal(s) and supporting objectives; (b) general plans (activities) to accomplish project goal(s); and (c) larger significance of the study
This capstone will build on the work done by \textcite{Montanari} on compositionally removing confusion in acyclic Petri nets, by exploring the possibility extend their method to locally finite Petri nets. To do so, we will compare their work to the dynamic method by \textcite{AB_branching_cells}, which does work for locally finite Petri nets. The focus of the analysis will be on identifying the complications caused by cycles in the net.
\end{abstract}

\keywords{True concurrency, Petri nets, event structures, confusion, Structural Branching Cells}

\section{Introduction}
% A brief description of the project, including the rationale, research objectives and questions to be addressed. The broader relevance of the research question and/or thesis should also be addressed, not only across the academic field, but also beyond it in terms of societal, cultural, environmental, political and/or interdisciplinary implications.
Many natural processes are fundamentally concurrent. For example, complex chemical reactions consist of many smaller concurrent reactions. To be able to analyse these processes formally, a mathematical framework is needed. This framework can in turn be used to, for example, determine properties of the system or to simulate the system. In addition, a general theory of concurrency allows the same mathematical tools to be applied to any concurrent system in any scientific field.

% Concurrent processes not only provide a lot of features, but also introduce some complications in dealing with them. In concurrent computer processes, for example, \emph{mutual exclusion locks} must be employed to avoid data corruption~\cite{Dijkstra}. As a result, a computational cluster might get stuck in \emph{deadlock}, where different parts of a program are waiting for one another to free up certain resources indefinitely~\cite{deadlock}.

One such mathematical framework is the theory of Petri nets. As introduced by C.\,A. Petri in \citeyear{Petri}~\cite{Petri}, Petri nets consist of a set of \emph{places}, a set of \emph{transitions} and a \emph{flow relation} connecting the two. Places model conditions in the system. When a place is \emph{marked} the condition holds in the current state. Transitions model the possibility of the system to change its state. If all inputs of a transition are marked, it is called \emph{enabled}. The transition can then \emph{fire}, taking away the markings of its inputs and marking the places of its output. Whenever multiple transitions share at least one place in their input, a choice between these transitions has to be made and they are said to be in \emph{conflict}. Graphically, places and transitions are represented by circles and rectangles, as shown in \fref{example_petri_net}. The dots in the two places at the top indicates that they are marked.

\begin{figure}[htbp]
    \centering
    \begin{subfigure}[t]{0.3\textwidth}
        \begin{tikzpicture}[>=stealth,bend angle=45,auto, x=0.7cm, y=0.9cm]
            \draw (1,4) node[place,tokens=1] (a) {}
                (4,4) node[place,tokens=1] (b) {}
                (0,3) node[transition] (t1) {$t_1$}
                (2,3) node[transition] (t2) {$t_2$}
                (4,3) node[transition] (t3) {$t_3$}
                (0,2) node[place] (c) {}
                (2,2) node[place] (d) {}
                (4,2) node[place] (e) {}
                (3,1) node[transition] (t4) {$t_4$}
                (3,0) node[place] (f) {};

            \begin{pgfonlayer}{background}
                \draw (a) edge[post] (t1);
                \draw (a) edge[post] (t2);
                \draw (b) edge[post] (t3);
                \draw (t1) edge[post] (c);
                \draw (t2) edge[post] (c);
                \draw (t2) edge[post] (d);
                \draw (t3) edge[post] (e);
                \draw (d) edge[post] (t4);
                \draw (e) edge[post] (t4);
                \draw (t4) edge[post] (f);
            \end{pgfonlayer}
        \end{tikzpicture}
    \caption{In the initial net, transitions $t_1$, $t_2$ and $t_3$ are enabled.}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.3\textwidth}
        \begin{tikzpicture}[>=stealth,bend angle=45,auto, x=0.7cm, y=0.9cm]
            \draw (1,4) node[place] (a) {}
                (4,4) node[place] (b) {}
                (0,3) node[transition] (t1) {$t_1$}
                (2,3) node[transition] (t2) {$t_2$}
                (4,3) node[transition] (t3) {$t_3$}
                (0,2) node[place, tokens=1] (c) {}
                (2,2) node[place, tokens=1] (d) {}
                (4,2) node[place, tokens=1] (e) {}
                (3,1) node[transition] (t4) {$t_4$}
                (3,0) node[place] (f) {};

            \begin{pgfonlayer}{background}
                \draw (a) edge[post] (t1);
                \draw (a) edge[post] (t2);
                \draw (b) edge[post] (t3);
                \draw (t1) edge[post] (c);
                \draw (t2) edge[post] (c);
                \draw (t2) edge[post] (d);
                \draw (t3) edge[post] (e);
                \draw (d) edge[post] (t4);
                \draw (e) edge[post] (t4);
                \draw (t4) edge[post] (f);
            \end{pgfonlayer}
        \end{tikzpicture}
    \caption{After firing $t_2$ and $t_3$, the transition $t_1$ is no longer enabled, but $t_4$ has become enabled.}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.3\textwidth}
        \begin{tikzpicture}[>=stealth,bend angle=45,auto, x=0.7cm, y=0.9cm]
            \draw (1,4) node[place] (a) {}
                (4,4) node[place] (b) {}
                (0,3) node[transition] (t1) {$t_1$}
                (2,3) node[transition] (t2) {$t_2$}
                (4,3) node[transition] (t3) {$t_3$}
                (0,2) node[place, tokens=1] (c) {}
                (2,2) node[place] (d) {}
                (4,2) node[place] (e) {}
                (3,1) node[transition] (t4) {$t_4$}
                (3,0) node[place, tokens=1] (f) {};

            \begin{pgfonlayer}{background}
                \draw (a) edge[post] (t1);
                \draw (a) edge[post] (t2);
                \draw (b) edge[post] (t3);
                \draw (t1) edge[post] (c);
                \draw (t2) edge[post] (c);
                \draw (t2) edge[post] (d);
                \draw (t3) edge[post] (e);
                \draw (d) edge[post] (t4);
                \draw (e) edge[post] (t4);
                \draw (t4) edge[post] (f);
            \end{pgfonlayer}
        \end{tikzpicture}
    \caption{After firing $t_4$, no more transitions are enabled.}
    \end{subfigure}
    \caption{An example of the execution of a Petri net. Places are represented by boxes and transitions are represented by circles. The dots indicate the places that are marked. If all inputs of a transition are marked, it is enabled and can fire. Firing a transition removes all markings of its inputs and marks all its outputs.}
    \label{fig:example_petri_net}
\end{figure}

In contrast with sequential processes, the order of execution of a concurrent process is inherently non-deterministic. Thus, equivalent orders of execution, or \emph{interleaving traces}, must be taken into account~\cite{Mazurkiewicz}. While this method is effective in many cases, some generality is lost when considering timed systems. Adding time to a Petri net results in the theory of Stochastic Petri Nets, however, doing so changes the dynamics of the system such that properties from the original net might not hold for the timed net~\cite[p.~6]{Bause}. 

In contrast, \emph{true-concurrent} semantics do not attempt to linearise the concurrent processes. As a result, time cannot be specified by a global clock, but is instead only defined locally, yielding a \emph{local partially ordered time}. In this model, the timing between concurrent events is not specified.

Probabilities can be used to extend Petri nets in several ways. We will focus on assigning probabilities to the choices in the net in order to model non-determinism using true-concurrent semantics. Sometimes, this can be done using the method described by \textcite{Varacca}. However, this method does not work when the net is \emph{confused}, meaning that parts of the net have dependent probabilities. There are two fundamental cases of confusion: asymmetric and symmetric, shown in \fref{confusion}. A net is called confused if it contains at least one of these structures.

\begin{figure}[htbp]
    \centering
    \begin{subfigure}[b]{0.48\textwidth}
        \centering
        \begin{tikzpicture}[>=stealth,bend angle=45, x=0.7cm, y=0.9cm]
            \draw (0,4) node[place,tokens=1] (a) {}
                (0,3) node[transition] (t1) {$t_1$}
                (0,2) node[place] (b) {}
                (2,2) node[place, tokens=1] (c) {}
                (1,1) node[transition] (t2) {$t_2$}
                (3,1) node[transition] (t3) {$t_3$}
                (1,0) node[place] (d) {}
                (3,0) node[place] (e) {}; 

            \begin{pgfonlayer}{background}
                \draw (a) edge[post] (t1);
                \draw (t1) edge[post] (b);
                \draw (b) edge[post] (t2);
                \draw (c) edge[post] (t2);
                \draw (c) edge[post] (t3);
                \draw (t2) edge[post] (d);
                \draw (t3) edge[post] (e);
            \end{pgfonlayer}
        \end{tikzpicture}
        \caption{Asymmetric confusion.}
        \label{fig:sym_confusion}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.48\textwidth}
        \centering
        \begin{tikzpicture}[>=stealth,bend angle=45, x=.7cm, y=0.9cm]
            \draw (1,2) node[place, tokens=1] (a) {}
                (3,2) node[place, tokens=1] (b) {}
                (0,1) node[transition] (t1) {$t_1$}
                (2,1) node[transition] (t2) {$t_2$}
                (4,1) node[transition] (t3) {$t_3$}
                (0,0) node[place] (d) {}
                (2,0) node[place] (e) {}
                (4,0) node[place] (f) {};
            
            \begin{pgfonlayer}{background}
                \draw (a) edge[post] (t1);
                \draw (a) edge[post] (t2);
                \draw (b) edge[post] (t2);
                \draw (b) edge[post] (t3);
                \draw (t1) edge[post] (d);
                \draw (t2) edge[post] (e);
                \draw (t3) edge[post] (f);
            \end{pgfonlayer}
        \end{tikzpicture}
        \caption{Symmetric confusion.}
        \label{fig:asym_confusion}
    \end{subfigure}
    \caption{The two fundamental cases of confusion in Petri nets.}
    \label{fig:confusion}
\end{figure}

A finite history of the execution of a Petri net can be represented in an \emph{occurrence graph}, which is an unfolding of the initial net. To generate this unfolding, every loop is replaced by a copy of the reachable net. The unfoldings of the nets in \fref{finiteness} are shown in \fref{unfoldings}.

Of particular interest to our analysis are locally finite Petri nets, which are nets containing loops where each transition is in conflict with finitely many other transitions in the unfolding of the net~\cite{AB_branching_cells}. The difference between locally finite nets and non-locally finite cyclic nets is illustrated in \fref{finiteness}.
A dynamic method for removing confusion in locally finite nets was developed by \textcite{AB_branching_cells}. For acyclic nets, a static method exists as well \cite{Montanari}. The goal of this capstone is to investigate how this method might be extended to locally finite nets.

\begin{figure}[htbp]
    \centering
    \begin{subfigure}[t]{0.48\textwidth}
        \centering
        \trimbox{1cm 1.7cm 0cm 0cm}{
            \begin{tikzpicture}[>=stealth,bend angle=45, x=0.7cm, y=0.9cm]
                \draw (1,2) node[place,tokens=1] (a) {}
                    (0,1) node[transition] (t1) {$t_1$}
                    (2,1) node[transition] (t2) {$t_2$}
                    (2,0) node[place] (b) {};

                \begin{pgfonlayer}{background}
                    \draw (a) edge[post] (t1);
                    \draw (t1) edge[post, out=270, in=180, looseness=5] (a);
                    \draw (a) edge[post] (t2);
                    \draw (t2) edge[post] (b);
                \end{pgfonlayer}
            \end{tikzpicture}
        }
        \caption{A locally finite Petri net with cycles.}
        \label{fig:locally_finite}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.48\textwidth}
        \centering
        \trimbox{1cm 1.7cm 0cm 0cm}{
            \begin{tikzpicture}[>=stealth,bend angle=45, x=.7cm, y=0.9cm]
                \draw (1,2) node[place, tokens=1] (a) {}
                    (3,2) node[place, tokens=1] (b) {}
                    (0,1) node[transition] (t1) {$t_1$}
                    (2,1) node[transition] (t2) {$t_2$}
                    (4,1) node[transition] (t3) {$t_3$}
                    (2,0) node[place] (d) {}
                    (4,0) node[place] (e) {};
                
                \begin{pgfonlayer}{background}
                    \draw (a) edge[post] (t1);
                    \draw (a) edge[post] (t2);
                    \draw (b) edge[post] (t2);
                    \draw (b) edge[post] (t3);
                    \draw (t1) edge[post, out=270, in=180, looseness=5] (a);
                    \draw (t2) edge[post] (d);
                    \draw (t3) edge[post] (e);
                \end{pgfonlayer}
            \end{tikzpicture}
        }
        \caption{A non-locally finite Petri net with cycles.}
        \label{fig:nonlocally_finite}
    \end{subfigure}
    \caption{A locally finite Petri net and a non-locally finite Petri net.}
    \label{fig:finiteness}
\end{figure}

\begin{figure}[htbp]
    \centering
    \begin{subfigure}[t]{0.48\textwidth}
        \centering
        \begin{tikzpicture}[>=stealth,bend angle=45, x=0.7cm, y=0.9cm]
            \draw (2,4) node[place,tokens=1] (a) {}
                (1,3) node[transition] (t1) {$t_1$}
                (3,3) node[transition] (t2) {$t_2$}
                (1,2) node[place] (b) {}
                (3,2) node[place] (c) {}
                (0,1) node[transition] (t3) {$t_1$}
                (2,1) node[transition] (t4) {$t_2$}
                (0,0) node[inner sep=-0.07cm] (d) {$\vdots$}
                (2,0) node[place] (e) {};

            \begin{pgfonlayer}{background}
                \draw (a) edge[post] (t1);
                \draw (a) edge[post] (t2);
                \draw (t1) edge[post] (b);
                \draw (t2) edge[post] (c);
                \draw (b) edge[post] (t3);
                \draw (b) edge[post] (t4);
                \draw (t3) edge[post] (d);
                \draw (t4) edge[post] (e);
            \end{pgfonlayer}
        \end{tikzpicture}
        \caption{Partial unfolding of the net in {\fref{locally_finite}}. There are no infinite transitions in conflict with infinite other transitions. Therefore the net is locally finite.}
        \label{fig:locally_finite_unfolding}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.48\textwidth}
        \centering
        \begin{tikzpicture}[>=stealth,bend angle=45, x=.7cm, y=0.9cm]
            \draw (2,4) node[place, tokens=1] (a) {}
                (4,4) node[place, tokens=1] (b) {}
                (1,3) node[transition] (t1) {$t_1$}
                (3,3) node[transition] (t2) {$t_2$}
                (5,3) node[transition] (t3) {$t_3$}
                (1,2) node[place] (c) {}
                (3,2) node[place] (d) {}
                (5,2) node[place] (e) {}
                (0,1) node[transition] (t4) {$t_1$}
                (2,1) node[transition] (t5) {$t_2$}
                (0,0) node[inner sep=-0.07cm] (f) {$\vdots$}
                (2,0) node[place] (g) {};
            
            \begin{pgfonlayer}{background}
                \draw (a) edge[post] (t1);
                \draw (a) edge[post] (t2);
                \draw (b) edge[post] (t2);
                \draw (b) edge[post] (t3);
                \draw (t1) edge[post] (c);
                \draw (t2) edge[post] (d);
                \draw (t3) edge[post] (e);
                \draw (c) edge[post] (t4);
                \draw (c) edge[post] (t5);
                \draw (b) edge[post, bend left] (t5);
                \draw (t4) edge[post] (f);
                \draw (t5) edge[post] (g);
            \end{pgfonlayer}
        \end{tikzpicture}
        \caption{Partial unfolding of the net in {\fref{nonlocally_finite}}. The transition $t_3$ is in conflict with infinite copies of $t_2$, two of which are shown. Therefore the net is not locally finite}
        \label{fig:nonlocally_finite_unfolding}
    \end{subfigure}
    \caption{Partial unfoldings of the Petri nets in {\fref{finiteness}}.}
    \label{fig:unfoldings}
\end{figure}

\section{Research Context}
% In this section, students should situate their working thesis or research question within the scholarly discussion of their selected topic in the academic field. Key studies that have been used to generate the question or thesis should be identified, grouped and synthesized. Secondary questions arising from a survey of the approaches taken to the subject, or from specific studies, should be identified and tested against the thesis or question.
To model the performance of concurrent processes, Markovian theory can be applied to Petri nets. The theory of Stochastic Petri Nets achieves this by adding time (either discrete or continuous) to the system in the form of a \emph{global clock}~\cite{Bause}. However, this global clock leads to increased complexity in the theory. This is because interleavings of concurrent events are treated as distinct, even though there is no inherent property that distinguishes them. Identifying the equivalent interleavings instead requires a \emph{partially ordered global time}~\cite{Abbes}. When partially ordered global time is used, a net where equivalent local states are given equal probabilities is called a \emph{Markov net}~\cite{AB_markov}.

Markov nets can be constructed from confusion-free nets with the construction by \textcite{Varacca}. Complementing this approach, \textcite{AB_branching_cells} developed a dynamic solution to remove confusion from locally finite Petri net using Branching Cells, which are intuitively described as local decision points~\cite{AB_markov}. However, a static method would be preferred over a dynamic method. Therefore, \textcite{Montanari} have developed a corresponding static method to remove confusion based on Structural Branching Cells or \emph{S-cells}. Like Branching Cells, S-cells are local decision points, but they differ from Branching Cells in that they are defined statically. This algorithm has only been proven to work for acyclic nets \cite{Montanari}.

It is conjectured that cycles can be added when the tokens at the beginning of the cycle are renewed~\cite{Montanari}. To explore this conjecture, we will identify the differences between this method and the Branching Cells method. Ultimately, we will attempt to extend the S-cell method to locally finite nets.

\section{Methodology}
% In this section, students should identify and describe the discipline-specific or interdisciplinary methods required to conduct their analysis of the data and/or primary and secondary source materials in developing their research question or thesis. While these methods will vary across disciplines, students must demonstrate an awareness of the range of methods available and provide their rationale for the methods selected.

In order to build understanding on the subject, we will start by implementing the algorithms in question, namely the Branching Cell algorithm and the S-cell algorithm. This has been done before, for example by Maraschio\footnote{\url{http://remconf.di.unipi.it/index.html}}, whose implementation we will be studied for guidance. The rest of the research will be bibliographic, as we will study the proofs used to show that the methods in \cite{AB_branching_cells} and \cite{Montanari} are correctly removing confusion. In particular, we will focus on identifying the fundamental differences between the methods. Finally, we will attempt to extend the method of \cite{Montanari} to locally finite nets using mathematical proofs.

\section{Expected Writing Update}
For the writing update, I will implement a basic version of the S-cell algorithm to remove confusion. Additionally, I will write a first version of the sections on concurrency, Petri nets, Markov nets and event structures.

\section{Outline}
The main sources for each section are in parentheses next to the section name.
\begin{enumerate}
    \item Introduction

    \item Concurrency
    \begin{itemize}
        \item Concurrency in general
        \item Applications of concurrency
        \item Non-determinism
        \item Probability
        \item The problem with global clock solutions to probabilities
    \end{itemize}

    \item Petri Nets (\textcite{Petri}, \textcite{Montanari})
    \begin{itemize}
        \item Definition of a Petri net
        \item Persistent places
        \item Dynamic Petri net
        \item Definition of confusion
    \end{itemize}

    \item Markov Nets (\textcite{Abbes})
    \begin{itemize}
        \item Markov chains \& Markov property
        \item Interleaving traces (\textcite{Mazurkiewicz})
        \item Definition of a Markov net
        \item Assigning probabilities to confusion-free nets
    \end{itemize}

    \item Event Structures (\textcite{Nielsen})
    \begin{itemize}
        \item Definition of event structures
        \item Prefixes, configurations
        \item Definition of locally finite nets
    \end{itemize}

    \item Branching Cells (\textcite{AB_branching_cells})
    \begin{itemize}
        \item Definition of branching cells
        \item Branching Cell method for removing confusion
    \end{itemize}

    \item Structural Branching Cells (\textcite{Montanari})
    \begin{itemize}
        \item Definition of S-cells
        \item S-cell method for removing confusion
    \end{itemize}

    \item Conclusion
\end{enumerate}

\nocite{Montanari}
\nocite{AB_branching_cells}
\nocite{Bause}
\nocite{Abbes}
\nocite{AB_markov}
\nocite{deadlock}
\nocite{Bowman}
\nocite{Hoare}
\nocite{Mazurkiewicz}
\printbibliography

\end{document}
