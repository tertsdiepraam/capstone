
 # Custom dependency and function for nomencl package
add_cus_dep( 'nlo', 'nls', 0, 'makenlo2nls' );
sub makenlo2nls {
  system( "makeindex -s nomencl.ist -o \"$_[0].nls\" \"$_[0].nlo\"" );
}

$pdflatex = 'texcount -merge -1 -sum=1,0,0,0,0,0,0 %T -out=%R.sum; pdflatex --shell-escape %O %S';
