%%% Imports %%%
\usepackage[utf8]{inputenc}

% Bibliography
\usepackage[firstinits=true, maxcitenames=2, backend=biber]{biblatex}

% Mathematical
\usepackage{amsmath, amssymb, amsthm}
\usepackage{thmtools} % correct autorefnames with shared theorem counter
\usepackage{mathtools}
\usepackage{physics}
\usepackage{stmaryrd} % \llbracket, \rrbracket, \llparenthesis & \rrparenthesis
\usepackage{cmap} % makes mathematical symbols searchable
\usepackage{siunitx}

% Graphics
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{adjustbox}
\usepackage[all]{xy}

% Hyperref
%\usepackage[hidelinks]{hyperref}
%\usepackage{bookmark}

% Tools for custom commands
\usepackage{ifthen}
\usepackage{pgffor}
\usepackage{xparse}
\usepackage{xstring} % \IfInteger

% Formatting
\usepackage[position=bottom]{subcaption}


%%% External sources %%%
\addbibresource{../bibliography.bib}

\graphicspath{{../Images/}}

%%%% Title page commands %%%
\newcommand\email[1]{
    \href{mailto:#1}{\url{#1}}
}

% \newcommand\keywords[1]{
%     \vspace{5mm}\noindent
%     \small{\textbf{\textit{Keywords ---}} #1}
% }

%%% Names for \autoref %%%
\newcommand\sectionautorefname{Section}
\newcommand\subsectionautorefname{Subsection}

%%% Better overline command %%%
\makeatletter
\newsavebox\myboxA{}
\newsavebox\myboxB{}
\newlength\mylenA{}
\newcommand*\xoverline[2][0.75]{%
    \sbox{\myboxA}{$\m@th#2$}%
    \setbox\myboxB\null% Phantom box
    \ht\myboxB=\ht\myboxA%
    \dp\myboxB=\dp\myboxA%
    \wd\myboxB=#1\wd\myboxA% Scale phantom
    \sbox\myboxB{$\m@th\overline{\copy\myboxB}$}%  Overlined phantom
    \setlength\mylenA{\the\wd\myboxA}%   calc width diff
    \addtolength\mylenA{-\the\wd\myboxB}%
    \ifdim\wd\myboxB<\wd\myboxA%
       \rlap{\hskip 0.5\mylenA\usebox\myboxB}{\usebox\myboxA}%
    \else
        \hskip -0.5\mylenA\rlap{\usebox\myboxA}{\hskip 0.5\mylenA\usebox\myboxB}%
    \fi}

%%% Mathematical Short-hands %%%
\newcommand\NN{\mathbb{N}}
\newcommand\ZZ{\mathbb{Z}}
\newcommand\RR{\mathbb{R}}
\newcommand\CC{\mathbb{C}}
\newcommand\EE{\mathcal{E}}
\newcommand\PP{\mathcal{P}}
\renewcommand\SS{\mathbb{S}}

\newcommand\pre[1]{\prescript{\bullet}{}{#1}}
\newcommand\post[1]{#1^\bullet}
\newcommand\init[1]{\prescript{\circ}{}{#1}}
\newcommand\final[1]{#1^\circ}
\newcommand\diff{\setminus}
\newcommand\union{\cup}
\newcommand\Union{\bigcup}
\newcommand\intersect{\cap}
\newcommand\Intersect{\bigcap}
\newcommand\deq{\coloneqq}
\newcommand\evtcausality{\curlyeqprec}
\newcommand\invsymb{\raisebox{.2ex}{$\scriptscriptstyle-1$}}
\newcommand{\inv}[1]{\operatorname{{#1}^{\invsymb}}}
\newcommand\scell[1]{[#1]}
\DeclareMathOperator\Ch{\textsc{Ch}}
\DeclareMathOperator\trunc{\upharpoonright}
\DeclareMathOperator\BC{\textsc{bc}}
\DeclareMathOperator\DN{\textsc{dn}}
\DeclareMathOperator\DYN{\textsc{dyn}}
\DeclareMathOperator\Shadow{\mathcal{S}}
\DeclareMathOperator\Prob{\mathbb{P}}
\DeclareMathOperator\Pre{\mathsf{Pre}}

\DeclareMathOperator\Id{\mathrm{Id}}
\DeclareMathOperator\conflict{\#}
\DeclareMathOperator\dconflict{\#_0}
\DeclareMathOperator\iconflict{\#_\mu}
\DeclareMathOperator\dep\triangleleft
\DeclareMathOperator\bdep\triangleright
\DeclareMathOperator\prune{\textsc{prune}}
\DeclarePairedDelimiter\dynamic\llbracket\rrbracket
\DeclarePairedDelimiter\static\llparenthesis\rrparenthesis
\DeclarePairedDelimiter\config\lbrack\rbrack
\DeclarePairedDelimiter\configenable\lbrack\lbrack
\DeclarePairedDelimiter\cluster\langle\rangle
\newcommand\negp[1]{\xoverline{\mathbf{#1}}}
\newcommand\uniqueexists{\exists!}

\newcommand\Forall[1]{\forall #1\;\,}
\newcommand\Exists[1]{\exists #1\;\,}

\usepackage{lineno}

% just to make sure it exists
\providecommand\given{}
% can be useful to refer to this outside \Set
\newcommand\SetSymbol[1][]{%
  \nonscript\:#1\vert
  \allowbreak
  \nonscript\:
  \mathopen{}}
\DeclarePairedDelimiterX\Set[1]\{\}{%
  \renewcommand\given{\;\SetSymbol[\delimsize]\;}
  #1
}

\DeclarePairedDelimiter\Tuple()

\renewcommand\S{\Set*}
\newcommand\T{\Tuple*}

% Usage:
% \fire{marking1, transition1, marking2, transition2, etc.}
% \fire*{marking1, marking2, marking3, etc.}
% A * or + is treated as a special case of transition.
\NewDocumentCommand\fire{s m}{
  \IfBooleanTF{#1} {%
    \foreach \x [count=\xi] in {#2} {%
      \ifthenelse{\equal{\xi}{1}}{\x}{\xrightarrow{}\x}%
    }%
  }{%
    \foreach \x [count=\xi] in {#2} {%
      \ifthenelse{\isodd{\xi}}{\x}{%
        \ifthenelse{\equal{\x}{*}}{%
          \xrightarrow{}^*%
        }{%
          \ifthenelse{\equal{\x}{+}}{%
            \xrightright{}^+%
          }{%
            \xrightarrow{\x}%
          }
        }%
      }%
    }%
  }%
}

%%% Theorem & definition environments %%%
\newtheorem{thm}{Theorem}[section]
%\newtheorem{lemma}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{coro}[thm]{Corollary}
\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{assumption}{Assumption}[section]

%%% Tikz styles and macros %%%
\usetikzlibrary{arrows, shapes, automata, petri, decorations.pathreplacing}
\tikzset{
    place/.style={
      circle,
      draw=black,
      fill=white,
      minimum size=4mm,
    },
    persistent/.style={
      circle,
      draw=black,
      fill=white,
      minimum size=3mm,
      double=white,
      double distance=0.5mm,
    },
    transition/.style={
      rectangle,
      draw=black,
      fill=white,
      minimum width=8mm,
      minimum height=5mm,
    },
    dynamictransition/.style={
      rectangle,
      draw=black,
      dashed,
      fill=white,
      minimum width=8mm,
      minimum height=5mm,
    },
    confusion/.style={
      decorate,
      decoration={
        snake,
        amplitude=0.5mm,
        segment length=2mm,
      }
    }
}

\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}

\pgfsetlayers{background,main,foreground}

\tikzset{>=stealth, x=0.7cm, y=0.8cm}

\newcommand\p[3][]{
  \draw (#3) node[place, label={#1}] (#2) {};
}

\newcommand\pp[3][]{
  \draw (#3) node[persistent, label={#1}] (#2) {};
}

\renewcommand\t[3][]{
  \draw (#3) node[transition] (#2) {#1};
}

\newcommand\dt[3][]{
  \draw (#3) node[dynamictransition] (#2) {#1};
}

\newcommand\e[3][]{
  *[o]=<.9pc,.9pc>{\bullet}
}

\newcommand\flow[1]{
  \begin{pgfonlayer}{background}
    \foreach \x/\y in {#1} {
      \draw (\x) edge[post] (\y);
    }
  \end{pgfonlayer}
}

\newcommand\dflow[1]{
  \begin{pgfonlayer}{background}
    \foreach \x/\y in {#1} {
      \draw[shorten >=0.4mm,shorten <=0.4mm, thin, <->] (\x) edge (\y);
    }
  \end{pgfonlayer}
}
\newcommand\conf[1]{
  \begin{pgfonlayer}{background}
    \foreach \x/\y in {#1} {
      \draw (\x) edge[confusion] (\y);
    }
   
  \end{pgfonlayer}
}

\newcommand\marking[1]{
  \foreach \p in {#1} {
    \node [token] at (\p) {};
  }
}

\newcommand\boldnet[1]{
  \begin{scope}[very thick, post/.style={->, thick, >=stealth'}]
    #1
  \end{scope}
}

\newcommand\event{\bullet}
\newcommand\nameup[1]{\POS[]+<0pc,0.9pc>\drop{#1}}
\newcommand\namedown[1]{\POS[]-<0pc,0.9pc>\drop{#1}}
\newcommand\nameleft[1]{\POS[]-<0.9pc,0pc>\drop{#1}}
\newcommand\nameright[1]{\POS[]+<0.9pc,0pc>\drop{#1}}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% TeX-command-extra-options: "--shell-escape"
%%% End:
