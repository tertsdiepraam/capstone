\documentclass{beamer}

\usepackage{adjustbox}
\input{preamble}
\input{../Nets/nets}

\newcommand\boldoverlay[3]{%
  \only<#2>{\boldsymbol{#1}}\only<#3>{\rlap{\ensuremath{#1}}\phantom{\boldsymbol{#1}}}%
}

\newcommand\addauthor[4]{%
  {\tiny\textit{#1}}\\[-1mm]%
  #2\\[1mm]%
  {\tiny%
    #3\\[-3mm]%
    \email{#4}%
  }
}

\setbeamerfont{bibliography item}{%
  size=\small,%
  series=\normalfont}
\setbeamerfont{bibliography entry author}{%
  size=\small,%
  series=\normalfont}
\setbeamerfont{bibliography entry title}{%
  size=\small,%
  series=\bfseries}
\setbeamerfont{bibliography entry location}{%
  size=\small,%
  series=\normalfont}
\setbeamerfont{bibliography entry note}{%
  size=\small,%
  series=\normalfont}


% Theme settings
\usetheme[]{metropolis}
\usefonttheme{professionalfonts} % this gives us the nice familiar math font,
%because the default of this theme is truly awful

% Graphics options
\graphicspath{{../Images/}}

\title{Removing Confusion in Locally Finite Petri Nets}
\author{Terts Diepraam}
\institute{Amsterdam University College}
\titlegraphic{\includegraphics[width=\textwidth]{{auc logo2.png}}}
\date{\today}

\setbeamertemplate{title page}{
  \begin{minipage}[b][\paperheight]{\textwidth}
    \vfill%
    \begin{minipage}{0.8\textwidth}
      \usebeamertemplate*{title}
    \end{minipage}
    ~
    \begin{minipage}{.067\textwidth}
      \begin{flushright}
        \includegraphics[width=\textwidth]{{auc logo2.png}}
      \end{flushright}
      \vfill
    \end{minipage}
    \leavevmode
    \usebeamertemplate*{title separator}
    \vspace*{.5cm}
    \begin{minipage}{.4\textwidth}
      \begin{flushleft}
        \addauthor%
        {Author}%
        {Terts Diepraam}%
        {Amsterdam University College}%
        {terts.diepraam@gmail.com}%
      \end{flushleft}
    \end{minipage}
    \begin{minipage}{.4\textwidth}
      \begin{flushleft}
        \addauthor%
        {Supervisor}%
        {Dr. Lorenzo Galeotti}%
        {Amsterdam University College}%
        {lorenzo.galeotti@gmail.com}
      \end{flushleft}
    \end{minipage}
    \\[.5cm]
    \begin{minipage}[t]{.4\textwidth}
      \begin{flushleft}
        \addauthor%
        {Tutor}%
        {Dr. Cor Zonneveld}%
        {Amsterdam University College}%
        {c.zonneveld@auc.nl}%
      \end{flushleft}
    \end{minipage}
    \begin{minipage}[t]{.5\textwidth}
      \begin{flushleft}
        \addauthor%
        {Reader}%
        {Dr. Yurii Khomskii}%
        {Amsterdam University College\\Institute for Logic, Language and Computation}%
        {yurii@deds.nl}
      \end{flushleft}
    \end{minipage}
    \\[.5cm]
    \today
    \vfill
    \vspace*{1mm}
  \end{minipage}
}

\setbeamertemplate{title}{
  \raggedright%
  \linespread{1.0}%
  {\LARGE\metropolis@titleformat{Removing Confusion in\\Locally Finite Petri Nets}}%
  \par%
  \vspace*{0.5em}
}

\setbeamertemplate{section in toc}[circle]

\begin{document}
\frame{\titlepage}

\note{
  \begin{itemize}
    \item Not talking about locally finite
    \item We'll focus on acyclic.
    \item Focus on some simple cases.
  \end{itemize}
}

\begin{frame}{Contents}
  \tableofcontents
\end{frame}

\section{Concurrency}

\note{
  \begin{itemize}
    \item Let me set the stage
    \item I'm gonna talk a lot about concurrency
    \item Concurrency is all around us.
  \end{itemize}
}

\begin{frame}{Definition of Concurrency}
  A concurrent process is a process where the \emph{order of some events can be
  changed} without affecting the outcome.
\end{frame}

\begin{frame}{Examples of Concurrency}
  % Real example and computational example
  \begin{itemize}
    \item Traffic
    \item Image processing
  \end{itemize}
\end{frame}

\begin{frame}{True-Concurrency}
  % Definition and reason
  Concurrent events are \emph{partially ordered}.
  \pause%
 
  We could \emph{totally order} them, but that is often impractical or leads to
  unexpected results.
  \pause%

  Using the partial order is using \emph{true-concurrent} semantics.
\end{frame}

\section{Confusion}

\begin{frame}{Definition of Confusion}
  % Getting rid of confusion is the PROBLEM I'M SOLVING
  A concurrent process has \emph{confusion} if there are choices in the process
  that are dependent on each other.
\end{frame}

\note{
  \begin{itemize}
    \item This is the main focus of my capstone
  \end{itemize}
}

\begin{frame}{Examples of Confusion (1)}
  Two students have to write a paper. They each have 2 options.
  \begin{enumerate}
    \item Collaborate with the other student.
    \item Write the paper on their own.
  \end{enumerate}
  \pause%
  When one of them chooses option 2, then the other must also choose option 2.
  \pause%
 
  This is an example of \emph{symmetric} confusion.
\end{frame}

\begin{frame}{Examples of Confusion (2)}
  Same situation, but student initially $a$ plans on writing on their own. But
  student $b$ might reach out to $a$ to ask to collaborate.
  \pause%
 
  Student $a$ then get an additional choice when approached by student $b$.
  \pause%
 
  This is an example of \emph{asymmetric} confusion.
\end{frame}

\begin{frame}{Confusion and Randomization}
  This makes it hard to \emph{randomize} the process using local probabilities.
  \pause%
 
  How do we attach probabilities to these dependent choices?
  \pause%
 
  Two solutions:
  \begin{enumerate}
    \pause%
    \item We change the ``scheduling'' of the process, solving the confusion
      atomically~\cite{AB_branching_cells}.
    \pause%
    \item We remove the confusion, while preserving the set of possible
      firing sequences and then attach probabilities~\cite{Montanari}.
  \end{enumerate}
  These are equivalent~\cite{Montanari}.
\end{frame}

\note{
  \begin{itemize}
    \item In the thesis, I go through both.
    \item In this presentation, I'll detail only the second option.
  \end{itemize}
}

\section{Petri Nets: A Model of Concurrency}
\note{
  Let's make this concrete
}
% Definition & Example
\begin{frame}{Marked Petri net}
  \begin{defn}[Marked Petri net]
    A marked Petri net is a tuple $\T{ P,T,F,m }$ where
    \begin{itemize}
      \item $P$ is the set of places,
      \item $T$ is the set of transitions, which is disjoint from $P$,
      \item $F\subseteq (P\times T)\cup(T\times P)$ is the flow relation,
      \item $m\in\NN^P$ is a \emph{multiset} is the marking of places.
    \end{itemize}
  \end{defn}
  \pause%
  \emph{Preset} and \emph{postset} of $x\in P\union T$:
  \[ \pre x \deq \S{ y\given \T{y,x} \in F}, \qquad \post x \deq \S{ z\given \T{x,z} \in F }. \]
  \pause%
  \emph{Causality relation} is the transitive closure of $F$:
  \[ \preceq\ \deq F^*. \]
\end{frame}

\note{
  \begin{itemize}
    \item Essentially a bipartite graph
    \item Places are conditions that can hold
    \item Transitions allow the system to change its state
    \item Multiset can contain an element multiple times formalized as a function
    \item The marking specifies which conditions from the places currently hold
  \end{itemize}
}

\begin{frame}{Example of Firing}
  \begin{figure}
    \centering
    \adjustbox{height=7cm}{
    \begin{tikzpicture}
      \shopnet

      \onslide<2>{\boldnet{
        \p{}{a1}
        \p{}{a3}
        \p{}{b1}
        \p{}{b3}
        \p{}{s}
        \p{}{sa2}
        \p{}{sb2}
        \p{}{a4}
        \p{}{b4}
      }}

      \onslide<3>{\boldnet{
        \t[$t_1$]{a2}{0,5}
        \t[$t_2$]{b2}{4,5}
        \t[$t_3$]{sa1}{1,3}
        \t[$t_4$]{sa3}{1,1}
        \t[$t_5$]{sb1}{3,3}
        \t[$t_6$]{sb3}{3,1}
      }}

      \only<4>{\boldnet{
        \flow{
          a1/a2, a2/a3, a3/sa1, sa1/sa2, sa2/sa3, sa3/a4,
          b1/b2, b2/b3, b3/sb1, sb1/sb2, sb2/sb3, sb3/b4,
          s/sa1, s/sb1%
        }

        \begin{pgfonlayer}{background}
          \draw (sa3) edge[post, out=300, in=-90] (s);
          \draw (sb3) edge[post, out=-120, in=-90] (s);
        \end{pgfonlayer}%
      }}

      \onslide<5>{\boldnet{
        \p{}{a1}
        \p{}{b1}
        \p{}{s}
      }}

      \onslide<1->\marking{a1,s,b1}
    \end{tikzpicture}
    }
  \end{figure}
  \[ N = \T{
      \boldoverlay{P}{2}{1,3-},
      \boldoverlay{T}{3}{-2,4-},
      \boldoverlay{F}{4}{-3,5-},
      \boldoverlay{m}{5}{-4,6-}
    }\]
\end{frame}

\begin{frame}{Firing Transitions}
  \begin{defn}[Enabling \& Firing]
    A transition $t$ is \emph{enabled} at the marking $m$, written $\fire{m, t}$
    if $\pre t \subseteq m$.
    \onslide<2>{If a transition is \emph{fired}, written $\fire{m, t, m'}$, then
      $m' = (m\diff \pre t) \union \post t$.}
  \end{defn}
  \begin{figure}
    \begin{tikzpicture}
      \p{a1}{0,2}
      \p{b1}{2,2}
      \t[$t$]{t1}{1,1}
      \p{c1}{0,0}
      \p{d1}{2,0}

      \onslide<2>{
        \p{a2}{6,2}
        \p{b2}{8,2}
        \t[$t$]{t2}{7,1}
        \p{c2}{6,0}
        \p{d2}{8,0}

        \draw[thick,->] (3.2,1) -- (4.8,1);
      }
     
      \flow{a1/t1, b1/t1, t1/c1, t1/d1}
      \only<2>\flow{a2/t2, b2/t2, t2/c2, t2/d2}

      \marking{a1, b1}
      \onslide<2>\marking{c2, d2}
    \end{tikzpicture}
  \end{figure}
\end{frame}

\note{
  \begin{itemize}
    \item The number of places in preset and postset does not need to match.
  \end{itemize}
}

\begin{frame}{Example}
  \begin{figure}
    \centering
    \adjustbox{height=7cm}{
    \begin{tikzpicture}
      \shopnet

      \onslide<1>\marking{a1,s,b1}
      \onslide<2>\marking{a3,s,b1}
      \onslide<3>\marking{sa2,b1}
      \onslide<4>\marking{a4,s,b1}
      \onslide<5>\marking{a4,s,b3}
      \onslide<6>\marking{a4,sb2}
      \onslide<7->\marking{a4,s,b4}
    \end{tikzpicture}
    }
  \end{figure}
  \onslide<8>{
    \[
        \fire{m_0,t_1, m_1,t_3, m_2,t_4, m_3,t_2, m_4 ,t_5, m_5 ,t_6, m_6}
      \]
  }
\end{frame}

\note{
  \begin{itemize}
    \item We could have easily picked another firing sequence.
  \end{itemize}
}

\begin{frame}{Conflict}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \centering
      \begin{tikzpicture}
        \shopnet
        \onslide<2->\boldnet{
          \t[$t_3$]{}{sa1}
          \t[$t_5$]{}{sb1}
        }
        \onslide<3>\boldnet{
          \t[$t_4$]{}{sa3}
          \t[$t_6$]{}{sb3}
        }
        \onslide<1->\marking{a3,s,b3}
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.5\textwidth}
      \\[0.5em]
      A choice must be made between $t_3$ and $t_5$.
      \onslide<2->{
        \begin{defn}[Direct conflict]
          Two transitions $t_1$ and $t_2$ are in \emph{direct conflict}, written
          $t_1\dconflict t_2$, if
          \[ t_1\neq t_2 \wedge \pre t_1\intersect\pre t_2\neq\emptyset. \]
        \end{defn}
      }
      \onslide<3>{
        \begin{defn}[Conflict]
          Two transitions $t_1$ and $t_2$ are in \emph{conflict}, written
          $t_1\conflict t_2$, if
          \[ \Exists{e\preceq t_1}\Exists{e'\preceq t_2} e\dconflict e'. \]
        \end{defn}
      }
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Confusion in Petri Nets}
  \begin{columns}
    \begin{column}{0.4\textwidth}
      \begin{figure}[h!]
        \centering
        \begin{tikzpicture}
          \sconf
        \end{tikzpicture}
      \end{figure}
      \begin{figure}[h!]
        \centering
        \begin{tikzpicture}
          \aconf
        \end{tikzpicture}
      \end{figure}
    \end{column}
    \begin{column}{0.6\textwidth}
      \\[0.5em]
      A $1$-safe marked net $N$ has confusion if there exist a reachable
      marking $m$ and transitions $t$, $u$, $v$ and one of the following
      holds.
      \begin{enumerate}
        \item In the symmetric case,
          \begin{itemize}
            \item $t, u, v$ are enabled at $m$,
            \item $\pre t \intersect \pre u \neq \emptyset \neq \pre u \intersect \pre v$ and
            \item $\pre t \intersect \pre v = \emptyset$.
          \end{itemize}
        \item In the asymmetric case,
          \begin{itemize}
            \item $t$ and $v$ are enabled at $m$,
            \item $u$ is not enabled at $m$ but becomes enabled after the firing
              of $t$ and
            \item $\pre t \intersect \pre v = \emptyset$ and
              $\pre u \intersect \pre v \neq \emptyset$.
          \end{itemize}
      \end{enumerate}
    \end{column}
  \end{columns}
\end{frame}

\section{Structural Branching Cells}

\begin{frame}{Overview}
  We will construct a confusion-free Petri net, using the following steps:
  \begin{enumerate}
    \item Identify the loci of decisions, which we call \emph{structural
      branching cells} or \emph{s-cells}.
    \item Figure out all maximal firing sequences in the s-cells.
    \item Combine the them into a full confusion-free Petri net.
    \item \emph{Prune} the net.
  \end{enumerate}
\end{frame}

\note{
  \begin{itemize}
    \item I will not detail every step mathematically here.
    \item But I will go through an example.
    \item The real construction uses dynamic Petri nets inbetween, making the
      mathematics easier, but that's not relevant here.
  \end{itemize}
}

\begin{frame}{Running Example}
  \begin{figure}[htbp]
    \centering
    \begin{tikzpicture}
      \p[left:$a$]{a}{1,4}
      \t[$t_1$]{1}{0,3}
      \t[$t_2$]{2}{2,3}
      \p[left:$c$]{c}{2,2}
      \p[left:$d$]{d}{4,2}
      \t[$t_3$]{3}{1,1}
      \t[$t_4$]{4}{3,1}
      \t[$t_5$]{5}{5,1}
      \p[below:$b$]{b}{0,0}
      \p[below:$e$]{e}{1,0}
      \p[below:$f$]{f}{3,0}
      \p[below:$g$]{g}{5,0}

      \flow{
        a/1, a/2,
        1/b, 2/c,
        c/3, c/4, d/4, d/5,
        3/e, 4/f, 5/g%
      }

      \onslide<2>{
        \boldnet{
          \t[$t_2$]{}2
          \t[$t_4$]{}4
          \t[$t_5$]{}5
        }
      }

      \onslide<3>{
        \boldnet{
          \t[$t_3$]{}3
          \t[$t_4$]{}4
          \t[$t_5$]{}5
        }
      }


      \marking{a,d}

      \onslide<5>{
        \draw[thick,dashed] (-.7,2.6) rectangle (2.7,4.4);
        \draw[thick,dashed] (0.3,0.6) rectangle (5.7,2.4);
        \node at (3.2,4) {$\CC_1$};
        \node at (6.2,2) {$\CC_2$};
      }
    \end{tikzpicture}
  \end{figure}
\end{frame}

\note{
  \begin{itemize}
    \item Notice that both cases of confusion are in this example.
  \end{itemize}
}

\begin{frame}{Running Example: $\CC_2$}
  \begin{figure}
    \begin{tikzpicture}
      \p[left:$c$]{c}{2,2}
      \p[left:$d$]{d}{4,2}
      \t[$t_3$]{3}{1,1}
      \t[$t_4$]{4}{3,1}
      \t[$t_5$]{5}{5,1}
      \p[below:$e$]{e}{1,0}
      \p[below:$f$]{f}{3,0}
      \p[below:$g$]{g}{5,0}
      \flow{
        c/3, c/4, d/4, d/5,
        3/e, 4/f, 5/g%
      }
      \onslide<-4>\marking{d}
      \onslide<-3,5>\marking{c}
    \end{tikzpicture}
  \end{figure}
  \begin{figure}
    \makebox[\textwidth][c]{
      \onslide<2->{
        \begin{subfigure}[t]{.5\textwidth}
          \centering
          \begin{tikzpicture}
            \p[above:$c$]{c}{1,2}
            \p[above:$d$]{d}{4,2}
            \t[$t_4$]{4}{1,1}
            \t[$t_3,t_5$]{35}{4,1}
            \p[below:$f$]{f}{1,0}
            \p[below:$e$]{e}{3,0}
            \p[below:$g$]{g}{5,0}

            \onslide<3->{
              \pp[below:$\negp e$]{ne}{0,0}
              \pp[below:$\negp g$]{ng}{2,0}
              \pp[below:$\negp f$]{nf}{4,0}
            }
            \flow{
              c/4, c/35, d/4, d/35,
              35/e, 4/f, 35/g%
            }
            \only<3->\flow{4/ne, 35/nf, 4/ng}
          \end{tikzpicture}
        \end{subfigure}
      }
    \begin{subfigure}[t]{.6\textwidth}
      \begin{tikzpicture}
        \onslide<4->{
          \pp[above:$\negp c$]{nc}{.5,2}
          \p[above:$d$]{d}{3,2}
          \t{tnc}{.5,1}
          \pp{p5}{1.75,1}
          \t[$t_5$]{5}{3,1}
          \pp[below:$\negp e$]{ne}{0,0}
          \pp[below:$\negp f$]{nf}{1,0}
          \p[below:$g$]{g}{3,0}
        }
        \only<4->\flow{tnc/p5, nc/tnc, tnc/ne, tnc/nf, p5/5, d/5, 5/g}

        \onslide<5->{
          \p[above:$c$]{c}{4.5,2}
          \pp[above:$\negp d$]{nd}{7,2}
          \t[$t_3$]{3}{4.5,1}
          \pp{p3}{5.75,1}
          \t{tnd}{7,1}
          \p[below:$e$]{e}{4.5,0}
          \pp[below:$\negp f$]{nf}{6.5,0}
          \pp[below:$\negp g$]{ng}{7.5,0}
        }
        \only<5->\flow{c/3, 3/e, nd/tnd, tnd/p3, p3/3, tnd/nf, tnd/ng}
      \end{tikzpicture}
    \end{subfigure}
    }
  \end{figure}
  \begin{center}
    \onslide<3->{
      \emph{Persistent places} (with double border) will stay marked.\\
      Marking $\negp a$ means that $a$ cannot be marked anymore.
    }
  \end{center}
\end{frame}

\note{
  \begin{itemize}
    \item Look at $\CC_2$.
    \item We are gonna create two nets.
    \item Put a token in each \emph{initial} place.
    \item What can we do?
    \item $t_3$ and $t_5$ OR $t_4$.
    \item This symmetric confusion is easily solved.
    \item Then we add \emph{negative places}.
    \item They are \emph{persistent}: infinite amount of tokens.
  \end{itemize}
}

\begin{frame}{Running Example: Dynamic Net per S-Cell}
  \begin{figure}[htbp]
    \begin{subfigure}[t]{.45\textwidth}
      \centering
      \begin{tikzpicture}
        \p[above:$a$]{a}{1.5,2}
        \t[$t_1$]{1}{.5,1}
        \t[$t_2$]{2}{2.5,1}
        \p[below:$b$]{b}{0,0}
        \pp[below:$\negp c$]{nc}{1,0}
        \pp[below:$\negp b$]{nb}{2,0}
        \p[below:$c$]{c}{3,0}

        \flow{a/1, a/2, 1/b, 1/nc, 2/nb, 2/c}
      \end{tikzpicture}
      \caption{$T_{\text{pos}}$ for s-cell $\CC_1$.}%
      \label{fig:running3a}
    \end{subfigure}
    \begin{subfigure}[t]{.45\textwidth}
      \centering
      \begin{tikzpicture}
        \pp[above:$\negp a$]{na}{1,2}
        \t{tna}{1,1}
        \pp[below:$\negp b$]{nb}{0,0}
        \pp[below:$\negp c$]{nc}{2,0}

        \flow{na/tna, tna/nb, tna/nc}
      \end{tikzpicture}
      \caption{$T_{\text{neg}}$ for s-cell $\CC_1$.}%
      \label{fig:running3b}
    \end{subfigure}
    \vspace{1em}
    \makebox[\textwidth][c]{
    \begin{subfigure}[t]{.5\textwidth}
      \centering
      \begin{tikzpicture}
        \p[above:$c$]{c}{1,2}
        \p[above:$d$]{d}{4,2}
        \t[$t_4$]{4}{1,1}
        \t[$t_3,t_5$]{35}{4,1}
        \pp[below:$\negp e$]{ne}{0,0}
        \p[below:$f$]{f}{1,0}
        \pp[below:$\negp g$]{ng}{2,0}
        \p[below:$e$]{e}{3,0}
        \pp[below:$\negp f$]{nf}{4,0}
        \p[below:$g$]{g}{5,0}

        \flow{
          c/4, c/35, d/4, d/35,
          4/ne, 4/f, 4/ng,
          35/e, 35/nf, 35/g%
        }
      \end{tikzpicture}
      \caption{$T_{\text{pos}}$ for s-cell $\CC_2$.}%
      \label{fig:running3c}
    \end{subfigure}
    \begin{subfigure}[t]{.6\textwidth}
      \begin{tikzpicture}
        \pp[above:$\negp c$]{nc}{.5,2}
        \p[above:$d$]{d}{3,2}
        \t{tnc}{.5,1}
        \pp{p5}{1.75,1}
        \t[$t_5$]{5}{3,1}
        \pp[below:$\negp e$]{ne}{0,0}
        \pp[below:$\negp f$]{nf}{1,0}
        \p[below:$g$]{g}{3,0}

        \flow{tnc/p5, nc/tnc, tnc/ne, tnc/nf, p5/5, d/5, 5/g}

        \p[above:$c$]{c}{4.5,2}
        \pp[above:$\negp d$]{nd}{7,2}
        \t[$t_3$]{3}{4.5,1}
        \pp{p3}{5.75,1}
        \t{tnd}{7,1}
        \p[below:$e$]{e}{4.5,0}
        \pp[below:$\negp f$]{nf}{6.5,0}
        \pp[below:$\negp g$]{ng}{7.5,0}

        \flow{c/3, 3/e, nd/tnd, tnd/p3, p3/3, tnd/nf, tnd/ng}
      \end{tikzpicture}
      \caption{$T_{\text{neg}}$ for s-cell $\CC_2$.}%
      \label{fig:running3d}
    \end{subfigure}
    }
  \end{figure}
\end{frame}

\begin{frame}{Running Example: Combined Net}
  \begin{figure}[htbp]
    \centering
    \adjustbox{height=7cm}{
      \begin{tikzpicture}
        \onslide<1>{
          \pp[above:$\negp a$]{na}{0,8}
          \t{tna}{0,6}
          \pp[left:$\negp b$]{nb}{4,4}
          \pp[left:$\negp d$]{nd}{14,4}
          \t{tnc}{0,2}
          \pp{pnc}{2,2}
          \t[$t_3$]{3}{10,2}
          \pp{pnd}{12,2}
          \t{tnd}{14,2}
          \pp[below:$\negp g$]{ng}{2,0}
          \pp[below:$\negp f$]{nf}{8,0}
          \pp[below:$\negp e$]{ne}{12,0}
        }
       
        \p[above:$a$]{a}{4,8}
        \t[$t_1$]{1}{2,6}
        \t[$t_2$]{2}{6,6}
        \pp[left:$\negp c$]{nc}{0,4}
        \p[left:$b$]{b}{2,4}
        \p[left:$d$]{d}{6,4}
        \p[left:$c$]{c}{8,4}

        \t[$t_5$]{5}{4,2}
        \t[$t_4$]{4}{6,2}
        \t[$t_3,t_5$]{35}{8,2}

        \p[below:$g$]{g}{4,0}
        \p[below:$f$]{f}{6,0}
        \p[below:$e$]{e}{10,0}

        \flow{
          a/1, a/2,
          1/b, 1/nc, 2/c,
          c/4, c/35, d/4, d/35, d/5,
          4/f, 35/e, 35/g, 5/g%
        }

        \only<1>\flow{
          na/tna, 2/nb, tna/nc,
          c/3, nc/tnc, nd/tnd,
          tnc/pnc, tnd/pnd, pnc/5, pnd/3,
          3/e, tnc/ng, tnc/nf, 4/ne, 4/ng, 35/nf, tnd/ne, tnd/nf%
        }

        \only<2>\flow{nc/5}

        \marking{a,d}
      \end{tikzpicture}
    }
  \end{figure}
\end{frame}

\begin{frame}{Running Example: Pruned}
  \begin{figure}[htbp]
    \centering
    \begin{tikzpicture}[y=1cm]
      \p[above:$a$]{a}{2,4}

      \t[$t_1$]{1}{0,3}
      \p[right:$b$]{b}{2,3}
      \t[$t_2$]{2}{4,3}

      \pp[left:$\negp c$]{nc}{0,2}
      \p[left:$d$]{d}{2,2}
      \p[left:$c$]{c}{4,2}

      \t[$t_5$]{5}{0,1}
      \t[$t_3,t_5$]{35}{2,1}
      \t[$t_4$]{4}{4,1}

      \p[below:$g$]{g}{0,0}
      \p[below:$e$]{e}{2,0}
      \p[below:$f$]{f}{4,0}

      \flow{
        a/1, a/2,
        1/b, 1/nc, 2/c,
        nc/5, d/5, d/35, d/4, c/35, c/4,
        5/g, 35/g, 35/e, 4/f%
      }

      \marking{a,d}
    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}{Recap}
  \begin{itemize}
      \pause%
    \item Concurrency is everywhere.
      \pause%
    \item Confusion makes randomizing a process difficult.
      \pause%
    \item Confusion can be removed by scheduling the net.
      \pause%
    \item A net can be scheduled by changing the structure.
  \end{itemize}
\end{frame}

\begin{frame}{Questions}
  \tableofcontents%
\end{frame}

\begin{frame}[t,allowframebreaks]{Bibliography}
  \printbibliography[heading=none]%
\end{frame}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-command-extra-options: "--shell-escape"
%%% End:
